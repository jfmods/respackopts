# Toggle Files
A condition can be looked at like an `IF` statement simply returning true/false to configure whether the texture/file should be loaded into the pack.
(Turns the texture "on" or "off").
You will need the `Pack ID` and `Entry Name` from your `respackopts.json5` that you created earlier
if you have not, see [Main Config](../setup/MainConfig.md) on how to do so.
<br>

You will need to navigate to the file you would like to toggle inside your resource, and create a `.rpo` file for it in the same folder(directory).
<br>

### Examples:
`texture.png` would be `texture.png.rpo`<br>
`recipe.json` would be `recipe.json.rpo`
<br>

### Layout:
```json
{
    condition: "<entry name>"
}
```
### Example:
```json
{
    condition: "someTexture"
}
```

For more information on conditions, [read this](../additional/AdvancedConditions.md)

### Toggle Directories
Directories ("Folders") can also be toggled without toggling individual files.
To do this, create a file in the directory you want to toggle titled `.rpo` (no file name).
Both conditions and fallbacks work like they do on files, however, specifying multiple fallbacks
for a directory will not work. In some cases, you may need to add `"DirFilterAdditive"`
to your `capabilities` inside your respackopts.json5 for fallbacks to work as expected.
Please be aware that you should NOT specify it unless you are SURE you need it, as it will worsen performance.

See [Switch between two files](./ToggleFilesWithFallback.md) to see how to swap textures out with each other (alternate Options)
