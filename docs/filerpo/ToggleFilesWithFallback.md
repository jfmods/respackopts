# Switch between two files
Imagine a simple `IF` statement controlling whether the texture/file should be loaded into the pack.
That is exactly what this page is about.
You will need the `Pack ID` and `Entry Name` from your `respackopts.json5` that you created earlier
if you have not, see [Main Config](../setup/MainConfig.md) on how to do so.
<br>

You will need to navigate to the file you would like to toggle inside your resource, and create a `.rpo` file for it in the same directory (folder).
<br>

### Examples:
`texture.png` would be `texture.png.rpo`<br>
`SomeRecipe.json` would be `SomeRecipe.json.rpo`
<br>

### Layout:
```json
{
    condition: "<entry name>",
    fallback: "location/of/the/file"
}
```
### Example:
```json
{
    condition: "someTexture",
    fallback: "assets/minecraft/textures/example/alternate.png"
}
```

Please be aware that Minecraft restricts file and path names.
You MUST follow these! If you don't (for example by using uppercase letters or symbols), your pack WILL NOT WORK!

## Another way to do this:
Respackopts supports specifying multiple possible fallbacks when configuring single files.
You can use this functionality as follows:
```json
{
    condition: "<entry name>",
    fallbacks: [
      "location/of/the/file",
      "assets/minecraft/textures/example/alternate.png"
    ]
}
```

```json
{
    condition: "someTexture"
}
```

See [Toggle Files ](./ToggleFiles.md) to see how to just disable files.
