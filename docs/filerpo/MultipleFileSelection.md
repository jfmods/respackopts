# Select one of multiple files
This Allows you to pick between multiple files (3 or more files) using similar logic to [Toggle Files](./ToggleFiles.md)
But this requires multiple `.rpo` files to achieve the desired goal.
You will need the `PackID` and `EntryName` from your `respackopts.json5` that you created earlier
if you have not, see [Main Config](../setup/MainConfig.md) on how to do so.
<br>

You will need to navigate to the file you would like to toggle inside your resource, and create a `.rpo` file for it in the same folder (directory).
<br>

### Examples:
`texture.png` would be `texture.png.rpo`<br>
`some_recipe.json` would be `some_recipe.json.rpo`<br>

### `.rpo` explanation :
You need a `.rpo` file per texture (except for the last one in your list) if you have 3 files to select between then you will need 2 `.rpo` files,
if you have 4 files to select between then you will need 3 `.rpo` files and so on etc...

### Layout:
This is your first `.rpo` with your first file:
```json
{
    condition: "<your entry>",
    fallback: [
      "path/to/second.json/.png",
      "path/to/third.json/.png"
    ]
}
```

This is your second `.rpo` with your second file:
```json
{
    condition: "<your entry>"
}
```

### Example:
```json
{
    condition: "option_one",
    fallback: [
      "path/to/secondfile.json",
      "path/to/thirdfile.json"
    ]
}
```

```json
{
    condition: "option_two"
}
```



