import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "ResPackOpts",
  description: "Documentation for the ResPackOpts config engine for Minecraft resource and data packs",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Modrinth', link: 'https://modrinth.com/mod/respackopts' },
      { text: 'CurseForge', link: 'https://www.curseforge.com/minecraft/mc-mods/respackopts' }
    ],

    sidebar: [
      {
        text: 'Setup',
        collapsed: false,
        items: [
          { text: 'Creating a Screen', link: '/setup/MainConfig.md' },
          { text: 'Advanced config entries', link: '/setup/AdvancedConfig.md' },
          { text: 'Translating your config', link: '/setup/Translations.md' },
          { text: 'Adding Presets', link: '/setup/Presets.md' },
          { text: 'Debugging', link: '/setup/Debugging.md' }
        ]
      },
      {
        text: 'Select files',
        collapsed: false,
        items: [
          { text: 'Toggle Files', link: '/filerpo/ToggleFiles.md' },
          { text: 'Switch between two files', link: '/filerpo/ToggleFilesWithFallback.md' },
          { text: 'Select one of multiple files', link: '/filerpo/MultipleFileSelection.md' }
        ]
      },
      {
        text: 'Additional',
        collapsed: false,
        items: [
          { text: 'Usage in Shader Packs', link: '/additional/Shaders.md' },
          { text: 'Advanced Conditions', link: '/additional/AdvancedConditions.md' },
          { text: 'Resource Expansion', link: '/additional/ResourceExpansion.md' },
          { text: 'Additional μScript tricks', link: '/additional/MuScript.md' },
          { text: 'Changes between versions', link: '/additional/Migrations.md' },
          { text: 'Packs using Respackopts', link: '/additional/UsedBy.md' }
        ]
      }
    ],

    socialLinks: [
      { icon: 'github', link: 'https://git.frohnmeyer-wds.de/JfMods/Respackopts' }
    ],

    search: {
      provider: "local"
    },
  },
  outDir: '../public',
  base: '/JfMods/Respackopts/',
  mpa: true,
  lang: 'en-US',
  markdown: {
    languages: [
      (async () => {
        const muscriptLanguage = (await import("syntax-muscript/syntaxes/muscript.tmLanguage.json", {
          with: {
            type: "json"
          }
        }) as any).default
        muscriptLanguage.name = "muscript"
        return muscriptLanguage
      })
    ]
  },
  sitemap: {
    hostname: 'https://pages.frohnmeyer-wds.de/JfMods/Respackopts/'
  }
})
