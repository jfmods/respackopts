---
layout: home

hero:
  name: "ResPackOpts"
  tagline: Documentation for the ResPackOpts config engine for Minecraft resource and data packs
  actions:
    - theme: brand
      text: Get Started
      link: /setup/MainConfig.md
    - theme: alt
      text: Examples
      link: /additional/UsedBy.md
      
features:
  - title: Resource Packs
    details: No more need for variants! Add toggles and sliders to your resource packs
    link: /filerpo/ToggleFiles.md
    linkText: Toggle Files
  - title: Shaders
    details: Add screens for your fabulous, Canvas and Frex shaders with full data access from GLSL
    link: /additional/Shaders.md
    linkText: Use from GLSL
  - title: Data Packs
    details: Manipulate and generate json, toggle files, and more
    link: /additional/ResourceExpansion.md
    linkText: Resource Expansion
---
