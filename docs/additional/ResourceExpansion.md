# Resource Expansion
Sometimes, you may wish to use respackopts values inside your text files (for example as model transforms),
without switching between a bunch of similar files. For this purpose, respackopts allows replacing text in files via resource expansion.

::: tip
If you want to use this to generate JSON, consider using the [JSON manipulation](./MuScript.md)
:::

You can use the `expansions` block in your .rpo to replace content with a value computed by executing a custom [μScript](https://git.frohnmeyer-wds.de/Johannes/java-commons/src/branch/master/muscript-runtime/src/test/resources/example.md) expression.
Values can be accessed in the script in the same exact way as in condition objects,
as the same language and object representation is used, though the result of a script is a string and not a boolean here.

::: tip
The result here is always a string.
Using string concatenation via the `||` operator is possible and recommended.

For example, the following is a valid script: `'Text ' || (someNumber * 15) || someBoolean`
:::

### Example:
respackopts.json5:
```json
{
  id: "examplePack",
  version: 9,
  capabilities: ["FileFilter", "DirFilter"],
  conf: {
    oakFence: {
      uvLock: true,
      invertOrientation: false,
      yFactor: 0
    }
  }
}
```
oak_fence.json.rpo:
```json
{
  expansions: {
    uvlock: "examplePack.oakFence.uvLock",
    orientation: "!examplePack.oakFence.invertOrientation",
    y000: "examplePack.oakFence.yFactor + 0",
    y090: "examplePack.oakFence.yFactor + 90",
    y180: "examplePack.oakFence.yFactor + 180",
    y270: "examplePack.oakFence.yFactor + 270"
  }
}
```
oak_fence.json:
```json
{
  "multipart": [
    {
      "apply": {
        "model": "minecraft:block/oak_fence_post"
      }
    },
    {
      "when": {
        "north": "${orientation}"
      },
      "apply": {
        "model": "minecraft:block/oak_fence_side",
        "y": ${y000},
        "uvlock": ${uvlock}
      }
    },
    {
      "when": {
        "east": "${orientation}"
      },
      "apply": {
        "model": "minecraft:block/oak_fence_side",
        "y": ${y090},
        "uvlock": ${uvlock}
      }
    },
    {
      "when": {
        "south": "${orientation}"
      },
      "apply": {
        "model": "minecraft:block/oak_fence_side",
        "y": ${y180},
        "uvlock": ${uvlock}
      }
    },
    {
      "when": {
        "west": "${orientation}"
      },
      "apply": {
        "model": "minecraft:block/oak_fence_side",
        "y": ${y270},
        "uvlock": ${uvlock}
      }
    }
  ]
}
```

## Explanation
The expansions block contains a key ("uvlock") and a script, which is executed when the file is loaded.
All instances of the key (formatted as ${name} -> ${lights} in this example) will be replaced by the result of the script.
Make sure to only use this feature on text files as unexpected results my occur otherwise
