::: info
The respackopts repository includes an example pack I use to test new features.

You can have a look at it [here](https://git.frohnmeyer-wds.de/JfMods/Respackopts/src/branch/master/run/resourcepacks/testpack) for semi-advanced examples of what you can do with respackopts.
:::

## Packs using Respackopts
This list doesn't include all packs using respackopts, just some I know of.

If you want your pack added/removed from here, you can open a PR

- [Redstone Tweaks](https://modrinth.com/resourcepack/redstone-tweaks)
- [Foliage+](https://www.planetminecraft.com/texture-pack/foliage-v-1-0/)
- [Stellar Tweaks](https://modrinth.com/resourcepack/stellar-tweaks)
- [YetAnotherGlint](https://modrinth.com/resourcepack/yetanotherglint)
- [GUI Retextures](https://www.curseforge.com/minecraft/texture-packs/gui-retextures)
- [Dark Containers](https://www.planetminecraft.com/texture-pack/dark-containers/)
- [Dark Transparent GUI](https://www.curseforge.com/minecraft/texture-packs/dark-transparent-gui)
- [Torturable Helthbars](https://modrinth.com/resourcepack/torturable-healthbars)
- [Vanilla PVP Edits](https://modrinth.com/resourcepack/pvp)
- [Stormilla](https://modrinth.com/resourcepack/stormilla)
- [Fresh Chests](https://www.planetminecraft.com/texture-pack/fresh-chests/)
- [Adapting Shulkers](https://modrinth.com/resourcepack/adapting-shulkers)
- [DefaultEx](https://www.curseforge.com/minecraft/texture-packs/defaultex)
- [Mods Russian Translation](https://modrinth.com/resourcepack/mods-ru)
- [GeekSMP](https://modrinth.com/resourcepack/geeksmp)

- [Lumi PBR Ext](https://github.com/spiralhalo/LumiPBRExt) (also Lumi Lights at some point)
- [Chimney Pots](https://www.curseforge.com/minecraft/texture-packs/chimney-pots)
- [Dirt path texture fix](https://www.curseforge.com/minecraft/texture-packs/dirt-path-texture-fix) ([planetminecraft](https://www.planetminecraft.com/texture-pack/dirt-path-texture-fix/))
- [Amethyst](https://www.curseforge.com/minecraft/modpacks/minecraft-enhanced-edition)'s internal pack
- [Better Chests](https://www.planetminecraft.com/texture-pack/better-chests-5414611/)
- [Shaped Like EGG](https://www.planetminecraft.com/texture-pack/shaped-like-egg/)
- Caliburn Leaves
