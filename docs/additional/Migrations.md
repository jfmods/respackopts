# Changes between versions
The `version` specified in your pack.json refers to a rpo format version.
A new rpo format version will be added whenever a change might break previous packs,
which will continue to use the old version.

This page lists the changes relevant to pack developers between versions and migrations (if needed)

## v1
Corresponds to version 1.0 - 1.4.4

- FREX Shader support
- Enum entries (no key translation yet)
- `respackopts.category.<pack>` to `respackopts.title.<pack>`
- Replace dots with underscores in frex definitions
- basic .rpo format
- `respackopts_loaded` in FREX shaders
- Slider support (`{"min": value, "default": value, "max": value}`)
- Tooltips: `respackopts.tooltip.<pack>.<entry>`
- Boolean conditions (and, or, nor, xor, equals): `{"<type>":[<entry 1>, <entry 2>]}`

## v2
Corresponds to version 2.0.0 - 2.2.0

- Removed LibCD compat
- Enum: define enum entries as booleans if they are selected, not their numerical value
- Further shader entry name sanitization

## v3
Corresponds to version 2.3.0

- `respackopts.title.<pack>` to `rpo.<pack>`
- `respackopts.field.<pack>.<entry>` to `rpo.<pack>.<entry>`
- `respackopts.tooltip.<pack>.<entry>` to `rpo.tooltip.<pack>.<entry>`

## v4
Corresponds to version 2.4.0 - 2.5.0

- Resource expansion support
- Directory .rpo support
- Allow using singular names and allow using a single entry instead of an array (`fallbacks` to `fallback`, `conditions` to `condition`, `expansions` to `expansion`)
- Remove requirement for specifying pack id in conditions: `<pack>:some.entry` to `some.entry`

## v5
Corresponds to version 2.6.0 - 2.7.1

- Pack capabilities, no longer includes DirFilter for new packs by default
- Add /rpo command for debugging

## v6
Corresponds to version 2.7.2 - 2.9.1

- allow specifying entries as objects with additional properties (`type`, `default`, `reloadType`)
- Fabulous shader support
- additional StarScript properties
- OPEN_ROOT support (allows custom icon.pngs among other things)

## v7
Corresponds to version 2.10.0

- Replace StarScript representation of enums

## v8
Corresponds to version 3.0.0-3.1.0

- Entirely new condition representation via MuScript
- Entirely new resource expansion using MuScript
- Removal of fabric conditions API interop in favor of enabling cleaner syntax

## v9
Corresponds to version 4.0.0-4.3.1

- New config screen backend powered by LibJF (needed to support serverside)
- New translation key syntax
- Removal of manual configuration for sliders vs input boxes
- Support for respackopts configs in the pack root (`/respackopts.json5` instead of `/assets/respackopts/conf.json`)

## v10
Corresponds to version 4.4.0

- Stricter enforcement of legal entry names: instead of sanitization, unsupported names are logged and ignored

## v11
Corresponds to version 4.5.0-4.5.1

- Directory RPOs in subdirectories are respected. Previously, only the innermost directory RPO would be used
- Multiple replacements when resolving fallbacks for directory RPOs are prevented

## v12
Corresponds to version 4.6.0-4.9.0

- Support for whole numbers (not integers!)

## v13
Corresponds to version 4.10.0

- An API for reading scripts and text from other assets in muScript expressions, allowing developers to factor out complex expressions and common code into separate files
- Support for string values
- An API for reading and writing json in muScript expressions, allowing developers to easily generate json objects and arrays for use in expansions
- Support for presets in `(assets|data)/respackopts/presets/` to allow users to easily switch between different configurations
