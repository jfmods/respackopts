# Usage in Shader Packs
## Getting started
All you need to do to access respackopts values is paste the following in a file where you want them according to your renderer:

| Renderer         | Include snippet                                                                                                   |
|------------------|-------------------------------------------------------------------------------------------------------------------|
| Canvas           | `#include respackopts:config_supplier`                                                                            |
| Vanilla/Fabulous | `//include respackopts:config_supplier` (this is done to avoid problems when loading shaders without respackopts) |

However, usually you will want to still have a pack that works if respackopts is not present. In that case, canvas will still load the file,
however, values that respackopts registers will not be available leading to compile errors. 
To avoid this, I recommend creating a source file in your own shader which will load default values if respackopts isn't present. You can do that as follows:
```glsl
#include respackopts:config_supplier

#ifndef respackopts_loaded
// define your default values here
// Example:
// #define examplePack_someTexture
#endif
```
You will then need to include this file in the places you want to access its values.

## Using the values
All values respackopts exposes follow the form: `<id>_<entry>` or `<id>_<category>_<entry>` 
To view the code respackopts generates for your pack, you can run the `/rpoc dump glsl` command in minecraft (You must enable the `debugCommands` config option for this). 
This will create a frex.frag file in your .minecraft/respackopts directory containing the generated shader code (available since 2.7.0).
