# Advanced Conditions
Respackopts uses "conditions" for deciding what files to load.
Respackopts conditions are usually a simple string with the entry name
(for example, `someTexture`),
however, respackopts provides various additional features to enhance their functionality.
This functionality is powered by the [μScript language](https://git.frohnmeyer-wds.de/Johannes/java-commons/src/branch/master/muscript/src/test/resources/example.md)
This page will provide a quick overview over the available operations.
For more complete documentation, use the above link.

## (Boolean) logic
You can use logic operations on several values to only show resources when a combination of them applies
Some common logical operations are supported, others indirectly.
That includes NOT (`!`), AND (`&`), OR (`|`), XOR (`==`) and XNOR (`!=`)

## Numbers
Numeric values can be accessed by their name, just like boolean values. All typical mathematical operations are supported,
that includes addition (`+`), subtraction (`-`), multiplication (`*`), division (`/`) and modulo (`%`)

## Accessing values
By default, all values for the current pack will be available by their name or as entries of a category (`someCategory.someOption`).
In addition, all values of other packs are accessible under their pack id (`somePack.someOption`).
Respackopts also allows checking mod (or minecraft) versions against predicates as follows:
`version('<mod name>', '<predicate>')`, for example `version('minecraft', '<=1.16.5')`

## Fabric Resource Conditions Interop
Fabric API provides a system called resource conditions, which is similar to respackopts own system.
Respackopts previously allowed accessing fabric resource conditions and respackopts conditions from one another,
however, this feature was removed in favor of enforcing the new muscript syntax.

## Dates
Respackopts supports expressing conditions on the date of resource loading (not necessarily the date the resource is used)

You can get the current date via `date.today` and compare it (`==`, `>`, `<`, ...) with a date object created
via the date constructor, for example: `date(2022, 5, 4)` stands for the fourth of may 2022.

You can also access the year, month or day using `.year`, `.month` or `.day` respectively (for example, `date.today.year == 2022`)

## Times
Respackopts supports expressing conditions on the time of resource loading (not necessarily the time the resource is used)

You can get the current time via `time.now` and compare it (`==`, `>`, `<`, ...) with a time object created
via the time constructor, for example: `date(23, 30, 1)` stands for half past 11 PM and one second

You can also access the hour, minute or second using `.hour`, `.minute` or `.second` respectively (for example, `time.now.hour >= 21`)
