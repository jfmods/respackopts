# Additional μScript tricks
Apart from all the things mentioned in [the condition docs](./AdvancedConditions.md),
the [language documentation](https://git.frohnmeyer-wds.de/Johannes/java-commons/src/branch/master/muscript-runtime/src/test/resources/example.md),
and the [standard library documentation](https://git.frohnmeyer-wds.de/Johannes/java-commons/src/branch/master/muscript-runtime/StandardLib.md),
Respackopts allows for some additional tricks that can be useful in various situations.

## Loading scripts
Respackopts provides your scripts with `RespackoptsFS`, allowing you to read scripts and text from other assets.

You can use `readString("<path>")` to read a text file from the assets, and `runScript("<path>")` to run a script from the assets.

This can be used to factor out complex conditions or expansions into more readable and maintainable scripts.

In these scripts, you can use `#include <path>` to include other scripts to further factor out common code.

A script might look like this:
```muscript
#include common.mu

getRotation = { yaw, pitch, roll ->
  toJson(
    listOf(
      listOf(cos(pitch) * cos(roll), sin(yaw) * sin(pitch) * cos(roll) - cos(yaw) * sin(roll), cos(yaw) * sin(pitch) * cos(roll) + sin(yaw) * sin(roll)),
      listOf(cos(pitch) * sin(roll), sin(yaw) * sin(pitch) * sin(roll) + cos(yaw) * cos(roll), cos(yaw) * sin(pitch) * sin(roll) - sin(yaw) * cos(roll)),
      listOf(-sin(pitch), sin(yaw) * cos(pitch), cos(yaw) * cos(pitch))
    )
  )
}

// the result of the last statement will be the result of running the script
// the following allows using the script in conditions as follows: runScript("path/to/script.mu").getRotation(yaw, pitch, roll)
{
  getRotation = getRotation
}
```

## JSON manipulation
Respackopts also provides your scripts with `JsonLib`, allowing you to easily generate JSON objects and arrays for use in expansions.

Just create your μScript data structure and pass it to `toJson` to get a JSON string.

Alternatively, you can use `fromJson` to parse a JSON string (which may have been loaded via `readString`) into a μScript data structure.

Keep in mind that this is potentially lossy, as JSON cannot represent all μScript data structures.