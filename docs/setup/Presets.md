::: info
Presets are only supported in packs using version 13 or newer.
If you are using an older version, please update your pack.
:::

## Presets are configs!
To create a preset, first set up your config the way you want it to be.

Then, to save it as a preset, simply copy the config file (which should be right next to your resource pack) to `/assets/respackopts/presets/` (or `data` for data packs) and rename it to `<presetName>.json5`.

That's it! You can now switch to your preset in the config screen.

## Presets are partial configs
Technically, presets do not have to be full configs.

You can create a preset that only contains a few entries,
and the rest will be kept as they were before switching to the preset.

Do keep in mind that this may lead to unexpected behavior if you are not careful.

For example, if you have a typo in your preset, the entry will not be applied and the user will not be notified of this.