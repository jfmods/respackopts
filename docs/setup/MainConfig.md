::: info
Any pack id or config entry name MUST only contain alphabetical characters (no numbers, spaces, underscores, etc) and SHOULD be in [camelCase](https://en.wikipedia.org/wiki/Camel_case).

If that is not the case, you may face unexpected issues.

You can add [translations](./Translations.md) to work around this in the UI shown to users though.
:::

### Creating a config
In order to start using Respackopts, you must first add a config file to your pack.
To do so, add a customized version of the following to your pack as `/respackopts.json5`:

```json
{
    id: "<PackID>",
    version: 9,
    capabilities: ["FileFilter", "DirFilter"],
    conf: {
        // Your config options here
    }
}
```

### Capabilities:
> **FileFilter:** (Default Enabled) <br> If stated will enable file filtering functionality.

> **DirFilter:** (Default Disabled) <br> If stated will enable folder filtering functionality.

> **DirFilterAdditive:** (Default Disabled) <br> If stated this will enable a compatibility fix for some mods that also modify resources.

## Adding a Toggle/Boolean
To add a boolean entry, add code like this: `"entryName": <Default Option (true/false)>` 

### Example:
```json
{
    id: "examplePack",
    version: 9,
    capabilities: ["FileFilter", "DirFilter"],
    conf: {
        someTexture: true,
        someOtherTexture: false
    }
}
```
### Result:
![configExampleBoolean](./img/ExamplePackBoolean.png "ConfigExampleBolean")

### See [ToggleFiles](../filerpo/ToggleFiles.md) to see the logic behind this

## Adding a Free Number Box
A number box follows the same principle as a boolean: `"entryName": Default Number`
*please note any value can be put in here with no limit*

### Example:
```json
{
    id: "examplePack",
    version: 9,
    capabilities: ["FileFilter", "DirFilter"],
    conf: {
        someOption: 10
    }
}
```
### Result:
![configExampleNumber](./img/ExamplePackNumber.png "ConfigExampleNumber")

## Adding a slider
A slider is slightly more complicated as a minimum and a maximum need to be defined.
More information is available [here](AdvancedConfig.md)

## Select From a list
To allow users to select one entry from a list, you can use a json array with string entries. *Numbers/etc are not supported*

### Example:
```json
{
    id: "examplePack",
    version: 9,
    capabilities: ["FileFilter", "DirFilter"],
    conf: {
        someOption: [
            "optionOne",
            "optionTwo",
            "optionThree"
        ]
    }
}
```
### Result:
![configExampleEnum](./img/ExamplePackEnum.png)

## Make a category

### Example:
```json
{
    id: "examplePack",
    version: 9,
    capabilities: ["FileFilter", "DirFilter"],
    conf: {
        someCategory: {
            someBoleanOption: true,
            someNumberOption: 5,
            someSelectionOption: [
                optionOne,
                optionTwo,
                optionThree
           ],
            someSliderOption: {
                min: 0,
                default: 5,
                max: 10
            }
        },
        someotherCategory: {
            someOtherBoleanOption: true,
            someOtherNumberOption: 5
        }
    }
}
```
### Result:
![configExampleCategory](./img/ExamplePackCategory.png)
