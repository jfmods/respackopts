## Common info
Every advanced entry will be represented as a json object.
Every entry can contain the following properties, regardless of its type:

| Name       | Required | Description                                                                                                                                               |
|------------|----------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| type       | yes      | The type of the entries. Each one can contain specific info, more on that below                                                                           |
| default    | yes      | The default value for this entry. This is what you would specify in a normal config entry (except for enums, where this is just the default entries name) |
| reloadType | no       | `"Resource"` or `"Simple"`, specifies whether resources have to be reloaded if this is changed. Frex shaders will be reloaded anyways                     |

## Strings
These entries will be displayed as text boxes. They can be used to drive any in-game text you might want to make configurable.
Their type is `string` or `text`.
They are most useful in combination with [Resource Expansion](../additional/ResourceExpansion.md), but you can also access them in your conditions.
See the muScript standard library documentation for more info on how to use and manipulate them.

## Numbers
There are two ways to display numbers: input boxes and sliders.
Any number input that provides a minimum and maximum value will be displayed as a slider instead of a box.
Their type is `number` for fractions and `integer` for whole numbers.

### Example:

```json
{
  id: "examplePack",
  version: 9,
  capabilities: [
    "FileFilter",
    "DirFilter"
  ],
  conf: {
    someOption: {
      type: "number",
      default: 5,
      min: 0,
      max: 10
    }
  }
}
```
### Result:
![configExampleSlider](./img/ExamplePackSlider.png)

## Booleans/Toggles
These do not expose any additional options, look under `Common info`. Their type is `boolean`

## Enum entries/Selecting from a list
The possible values for this entry will be under `values`, while the `default` value will just be a string.
Their type is `enum`

### Example:
```json
{
  id: "examplePack",
  version: 9,
  capabilities: ["FileFilter", "DirFilter"],
  conf: {
    someOption: {
      type: "enum",
      default: "Second",
      values: ["First", "Second", "Third"]
    }
  }
}
```
### Result:
![configExampleSlider](./img/ExamplePackEnum2.png)
