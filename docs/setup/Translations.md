# Translating your config
Adding a translation file to your pack can not only be used to translate your content, it also enables you to change the way entries are displayed in the config screen without changing your actual config. 

::: warning
By default, the mod will try to guess a nice name by itself, making this process harder.
Make sure you disable this by toggling of `generateNames` in the config screen!
:::

## Creating a lang file
Respackopts relies on minecrafts built-in system for translations. This means that you can add them to the normal `assets/minecraft/lang/en_us.json` file in which minecrafts on translations reside. 
An empty translation file will look like this:
```json
{
    // your entries
}
```

## Changing the config screens title
In order to change the title of the config screen respackopts generates for your pack, you can add an entry as follows:
```json
{
    "rpo.examplePack": "Some other name"
}
```
This will change the display name of the pack with the id `examplePack` to the piece of text after it, in this case `Some other name`.

## Changing an entries name
This works similarly to changing the title, however, the name of the entry is appended to the pack id. Example:
```json
{
    "rpo.examplePack": "Some other name",
    "rpo.examplePack.someTexture": "Some Texture"
}
```
This example would set the text of the entry named `someTexture` in the pack with the id `examplePack`. Categories work the same way as they do in .rpo files and everywhere else: you use `categoryName.entryName` instead of `entryName`.

Enum entries ("Select From a list") can be translated using `rpo.<pack id>.<entry id>.<entry>`, for example `"rpo.examplePack.someOption.optionOne": "Option One"`

## Adding a tooltip to an entry
The translation system also enables you to add tooltips (small pieces of text that are shown only if the users' mouse cursor is above the option) to config options. The syntax for these is as follows: `rpo.tooltip.<PackID>.<Entry Name>`. 
Example:
```json
{
    "rpo.examplePack": "Some other name",
    "rpo.examplePack.someTexture": "Some Texture",
    "rpo.examplePack.someTexture.tooltip": "This would be the tooltip"
} 
```

## Finished example
Assuming that someTexture is a boolean toggle, with the language file presented above, the config screen will look as follows:
![configExampleTranslated](./img/ExamplePackTranslations.png)