plugins {
    id("jfmod")
}

allprojects { group = "io.gitlab.jfronny" }
base.archivesName = "respackopts"

loom {
    accessWidenerPath.set(file("src/main/resources/respackopts.accesswidener"))
}

repositories {
    maven("https://maven.vram.io/") {
        name = "FREX/Canvas"
        content {
            includeGroup("io.vram")
        }
    }
    maven("https://api.modrinth.com/maven") {
        name = "Modrinth"
        content {
            includeGroup("maven.modrinth")
        }
    }
}

val muscriptVersion by extra("1.8.0-SNAPSHOT")
val modmenuVersion = "13.0.0-beta.1"
jfMod {
    minecraftVersion = "1.21.4"
    yarn("build.1")
    loaderVersion = "0.16.9"
    libJfVersion = "3.18.3"
    fabricApiVersion = "0.111.0+1.21.4"

    modrinth {
        projectId = "respackopts"
        requiredDependencies.addAll("fabric-api", "libjf")
        optionalDependencies.add("modmenu")
    }
    curseforge {
        projectId = "430090"
        requiredDependencies.addAll("fabric-api", "libjf")
        optionalDependencies.add("modmenu")
    }
}

dependencies {
    modImplementation("net.fabricmc.fabric-api:fabric-api")
    include(modImplementation("io.gitlab.jfronny:muscript-all:$muscriptVersion")!!)
    modImplementation("io.gitlab.jfronny:muscript-json:$muscriptVersion") {
        isTransitive = false
    }
    include("io.gitlab.jfronny:muscript-json:$muscriptVersion")
    modImplementation("io.gitlab.jfronny.libjf:libjf-data-manipulation-v0")
    modImplementation("io.gitlab.jfronny.libjf:libjf-config-core-v2")
    modImplementation("io.gitlab.jfronny.libjf:libjf-config-ui-tiny")
    modImplementation("io.gitlab.jfronny.libjf:libjf-resource-pack-entry-widgets-v0")

    val nofabric: Action<ExternalModuleDependency> = Action {
        exclude("net.fabricmc") // required to work around duplicate fabric loaders
    }
    modLocalRuntime("io.gitlab.jfronny.libjf:libjf-devutil", nofabric)
    modLocalRuntime("com.terraformersmc:modmenu:$modmenuVersion", nofabric)
    modClientCompileOnly("com.terraformersmc:modmenu:$modmenuVersion", nofabric)

    modClientCompileOnly("io.vram:frex-fabric:20.5.+")

    testImplementation("org.junit.jupiter:junit-jupiter:5.11.3")

    compileOnly("io.gitlab.jfronny:commons-serialize-generator-annotations:$muscriptVersion")
    testAnnotationProcessor(annotationProcessor("io.gitlab.jfronny:commons-serialize-generator:$muscriptVersion")!!)

    // Canvas for FREX testing
    //modClientRuntimeOnly("io.vram:canvas-fabric:20.2.+")

    // Sodium for debugging
    modClientRuntimeOnly("maven.modrinth:sodium:mc1.21.4-0.6.3-fabric")
}
