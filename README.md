# Respackopts
**PSA:** Depending on the version you are downloading, a separate installation of [cloth config](https://modrinth.com/mod/cloth-config), or [LibJF](https://modrinth.com/mod/libjf) may be required

Respackopts provides resource and data packs with config menus\
By default, it provides a custom system for conditional resources, resource expansion and integrates with frex, canvas and fabulous shaders

## Using Respackopts
### Users
After installing respackopts, a menu button will appear besides all supported resource packs.
No further configuration is needed.

### Pack authors
Please head to the [wiki](https://pages.frohnmeyer-wds.de/JfMods/Respackopts/) for details on how to use respackopts in a pack.

# Performance Impact
Performance always depends on factors other than the game itself like disk speed, resource usage by other programs, the OS you are using, etc.
These measurements were taken with Minecraft 1.20.1, Fabric Loader 0.14.21, Fabric API 0.84.0, LibJF 3.8.0 and 
Therefore, please do not assume these measurements to be accurate for all use cases.

LibJF injects hooks into the resource loader, meaning there is additional overhead for every resource access.
In practice, this results in a five percent increase in resource reloading time over vanilla and an eight percent increase in game load times over vanilla (.2 seconds).

Respackopts uses those hooks and additional mixins to work, adding additional overhead.
In practice, this results in a six percent increase in resource reloading time over vanilla and an eighteen percent increase in game load times over vanilla (.43 seconds).

The most significant increase is measurable during pack scanning, which takes nearly 100 times longer when comparing Minecraft with Respackopts to vanilla.
This is because Respackopts performs and caches most parsing and computation during this stage.
As the statistic for game loading shows, this is not necessarily noticeable during startup, because the total is still comparably small.
It is only noticeable in the resource pack screen, where scanning is performed every 20 ticks and whenever the screen is resized and the impact is much larger.
For this reason, I have created [a separate mod](https://modrinth.com/mod/async-pack-scan) that tweaks that screen for increased performance.
