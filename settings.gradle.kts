pluginManagement {
    repositories {
        maven("https://maven.frohnmeyer-wds.de/mirrors")
        maven("https://maven.neoforged.net/releases")
        gradlePluginPortal()
    }
    plugins {
        id("jfmod") version("1.7-SNAPSHOT")
    }
}

rootProject.name = "respackopts"
