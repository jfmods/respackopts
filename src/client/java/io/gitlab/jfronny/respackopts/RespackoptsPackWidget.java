package io.gitlab.jfronny.respackopts;

import com.mojang.blaze3d.systems.RenderSystem;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigInstance;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.DSL;
import io.gitlab.jfronny.libjf.config.api.v2.ui.ConfigScreenFactory;
import io.gitlab.jfronny.libjf.entrywidgets.api.v0.ResourcePackEntryWidget;
import io.gitlab.jfronny.respackopts.model.cache.CacheKey;
import io.gitlab.jfronny.respackopts.model.naming.BranchNaming;
import io.gitlab.jfronny.respackopts.model.tree.ConfigBranch;
import io.gitlab.jfronny.respackopts.util.MetaCache;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.pack.ResourcePackOrganizer;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.util.Identifier;

public class RespackoptsPackWidget implements ResourcePackEntryWidget {
    @Override
    public boolean isVisible(ResourcePackOrganizer.Pack pack, boolean selectable) {
        return MetaCache.getKeyByDisplayName(pack.getDisplayName().getString()) != null;
    }

    @Override
    public int getWidth(ResourcePackOrganizer.Pack pack) {
        return 20;
    }

    @Override
    public int getHeight(ResourcePackOrganizer.Pack pack, int rowHeight) {
        return 20;
    }

    @Override
    public void render(ResourcePackOrganizer.Pack pack, DrawContext context, int x, int y, boolean hovered, float tickDelta) {
        RenderSystem.setShaderColor(1, 1, 1, 1f);
        RenderSystem.disableDepthTest();
        context.drawTexture(RenderLayer::getGuiTextured, Identifier.of("modmenu", "textures/gui/configure_button.png"), x, y, 0, hovered ? 20 : 0, 20, 20, 32, 64);
        RenderSystem.enableDepthTest();
    }

    @Override
    public void onClick(ResourcePackOrganizer.Pack pack) {
        CacheKey dataLocation = MetaCache.getKeyByDisplayName(pack.getDisplayName().getString());
        if (dataLocation != null) {
            MinecraftClient c = MinecraftClient.getInstance();
            String id = MetaCache.getId(dataLocation);
            ConfigBranch cb = MetaCache.getBranch(dataLocation);
            ConfigInstance ci = DSL.create(id).config(builder -> cb.buildConfig(builder, dataLocation.dataLocation()));
            ConfigScreenFactory.Built<?> screen = ConfigScreenFactory.getInstance().create(ci, new BranchNaming(id, cb), c.currentScreen);
            c.setScreen(screen.get());
        }
    }
}
