package io.gitlab.jfronny.respackopts;

import io.gitlab.jfronny.respackopts.integration.FrexCompat;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.client.MinecraftClient;
import net.minecraft.server.integrated.IntegratedServer;
import net.minecraft.util.Identifier;

import java.util.concurrent.CompletableFuture;

@Environment(EnvType.CLIENT)
public class RespackoptsClient implements ClientModInitializer {
    public static final boolean FREX_LOADED = FabricLoader.getInstance().isModLoaded("frex");
    public static final Identifier RPO_SHADER_ID = Identifier.of(Respackopts.ID, "config_supplier");
    public static boolean forcePackReload = false;

    @Override
    public void onInitializeClient() {
        if (RespackoptsConfig.debugCommands)
            RpoClientCommand.register();
        if (FREX_LOADED) {
            FrexCompat.init();
        }
    }

    public static CompletableFuture<Void> forceReloadResources() {
        forcePackReload = true;
        if (RespackoptsConfig.debugLogs) Respackopts.LOGGER.info("Forcing resource reload");
        return CompletableFuture.allOf(MinecraftClient.getInstance().reloadResources());
    }

    public static CompletableFuture<Void> reloadIntegratedServerData() {
        IntegratedServer is = MinecraftClient.getInstance().getServer();
        if (is != null) {
            is.getDataPackManager().scanPacks();
            return is.reloadResources(is.getDataPackManager().getEnabledIds());
        }
        return CompletableFuture.completedFuture(null);
    }
}
