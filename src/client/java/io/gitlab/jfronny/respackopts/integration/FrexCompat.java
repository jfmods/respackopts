package io.gitlab.jfronny.respackopts.integration;

import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.RespackoptsClient;
import io.vram.frex.api.config.ShaderConfig;

public class FrexCompat {
    public static void init() {
        ShaderConfig.registerShaderConfigSupplier(RespackoptsClient.RPO_SHADER_ID, () -> ShaderIntegration.getShaderImportSource());
        Respackopts.LOGGER.info("enabled frex/canvas support");
    }

    public static void handleSave() {
        try {
            ShaderConfig.invalidateShaderConfig();
        } catch (RuntimeException e) {
            Respackopts.LOGGER.error("Could not reload shader config", e);
        }
    }
}
