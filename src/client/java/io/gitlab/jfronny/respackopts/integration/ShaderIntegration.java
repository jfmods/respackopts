package io.gitlab.jfronny.respackopts.integration;

import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.RespackoptsClient;
import io.gitlab.jfronny.respackopts.RespackoptsConfig;
import io.gitlab.jfronny.respackopts.util.MetaCache;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class ShaderIntegration implements SaveHook.Client {
    private static String shaderImportSource;

    public static String getShaderImportSource() {
        if (shaderImportSource == null) {
            Respackopts.LOGGER.error("Shader import source is null");
            return "";
        }
        return shaderImportSource;
    }

    @Override
    public CompletableFuture<Void> onSave(Arguments args) {
        if (RespackoptsConfig.debugLogs)
            Respackopts.LOGGER.info("Generating shader code");
        StringBuilder sb = new StringBuilder();
        sb.append("#ifndef respackopts_loaded");
        sb.append("\n#define respackopts_loaded");
        MetaCache.forEach((key, state) -> state.configBranch().buildShader(sb, state.packId()));
        sb.append("\n#endif");
        shaderImportSource = sb.toString();
        if (RespackoptsClient.FREX_LOADED) {
            FrexCompat.handleSave();
        }

        if (args.flagResourcesForReload()) {
            RespackoptsClient.forcePackReload = true;
        }
        List<CompletableFuture<Void>> futures = new ArrayList<>();
        if (args.reloadResourcesImmediately()) {
            futures.add(RespackoptsClient.forceReloadResources());
        }
        if (args.reloadData()) {
            futures.add(RespackoptsClient.reloadIntegratedServerData());
        }
        return CompletableFuture.allOf(futures.toArray(CompletableFuture[]::new));
    }
}
