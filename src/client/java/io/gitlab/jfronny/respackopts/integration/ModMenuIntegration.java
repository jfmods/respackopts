package io.gitlab.jfronny.respackopts.integration;

import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;
import io.gitlab.jfronny.libjf.config.api.v2.Naming;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.RespackoptsClient;
import io.gitlab.jfronny.respackopts.JFC_RespackoptsConfig;

public class ModMenuIntegration implements ModMenuApi {
    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        return parent -> {
            var built = io.gitlab.jfronny.libjf.config.api.v2.ui.ConfigScreenFactory.getInstance().create(JFC_RespackoptsConfig.INSTANCE, Naming.get(Respackopts.ID), parent);
            built.onSave(() -> {
                if (RespackoptsClient.forcePackReload) {
                    RespackoptsClient.forceReloadResources();
                }
            });
            return built.get();
        };
    }
}
