package io.gitlab.jfronny.respackopts.mixin;

import com.llamalad7.mixinextras.sugar.Local;
import io.gitlab.jfronny.respackopts.util.FallbackI18n;
import io.gitlab.jfronny.respackopts.util.OverlayMap;
import net.minecraft.client.resource.language.LanguageManager;
import net.minecraft.client.resource.language.TranslationStorage;
import net.minecraft.resource.ResourceManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.HashMap;
import java.util.Map;

@Mixin(LanguageManager.class)
public class LanguageManagerMixin {
    @Inject(method = "reload(Lnet/minecraft/resource/ResourceManager;)V", at = @At("TAIL"))
    private void rpo$appendTranslations(ResourceManager manager, CallbackInfo ci, @Local TranslationStorage translationStorage) {
        TranslationStorageAccessor storage = (TranslationStorageAccessor) translationStorage;
        Map<String, String> map = new HashMap<>();
        FallbackI18n.insertInto(map);
        storage.setTranslations(new OverlayMap<>(storage.getTranslations(), map));
    }
}
