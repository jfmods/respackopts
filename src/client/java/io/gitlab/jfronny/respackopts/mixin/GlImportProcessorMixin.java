package io.gitlab.jfronny.respackopts.mixin;

import io.gitlab.jfronny.respackopts.RespackoptsClient;
import io.gitlab.jfronny.respackopts.integration.ShaderIntegration;
import net.minecraft.client.gl.GlImportProcessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArg;

@Mixin(GlImportProcessor.class)
public class GlImportProcessorMixin {
    @Unique private static final String respackopts$prefix = "\n//include " + RespackoptsClient.RPO_SHADER_ID;
    @ModifyArg(method = "readSource(Ljava/lang/String;)Ljava/util/List;", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/gl/GlImportProcessor;parseImports(Ljava/lang/String;Lnet/minecraft/client/gl/GlImportProcessor$Context;Ljava/lang/String;)Ljava/util/List;"), index = 0)
    private String modify(String value) {
        return value.replace(respackopts$prefix, "\n" + ShaderIntegration.getShaderImportSource());
    }
}
