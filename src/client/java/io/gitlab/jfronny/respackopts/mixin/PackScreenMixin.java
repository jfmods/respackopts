package io.gitlab.jfronny.respackopts.mixin;

import io.gitlab.jfronny.respackopts.Respackopts;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.util.ArrayList;
import java.util.List;

// Workaround to prevent pack screen from repeatedly reloading resources once a pack config is updated
// (which happens every time resources are reloaded)
@Mixin(targets = "net.minecraft.client.gui.screen.pack.PackScreen$DirectoryWatcher")
public class PackScreenMixin {
    @Shadow @Final private Path path;

    @Redirect(method = "pollForChange()Z", at = @At(value = "INVOKE", target = "Ljava/nio/file/WatchKey;pollEvents()Ljava/util/List;"))
    private List<WatchEvent<?>> redirectPollEvents(WatchKey instance) {
        List<WatchEvent<?>> list = new ArrayList<>();
        for (WatchEvent<?> event : instance.pollEvents()) {
            Path path = this.path.resolve((Path) event.context());
            if (path.getFileName().toString().endsWith(Respackopts.FILE_EXTENSION)) continue;
            list.add(event);
        }
        return list;
    }
}
