package io.gitlab.jfronny.respackopts.mixin;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.RespackoptsConfig;
import io.gitlab.jfronny.respackopts.model.DiscoveredPack;
import io.gitlab.jfronny.respackopts.model.GC_PackMeta;
import io.gitlab.jfronny.respackopts.model.PackMeta;
import io.gitlab.jfronny.respackopts.model.cache.CacheKey;
import io.gitlab.jfronny.respackopts.util.FallbackI18n;
import io.gitlab.jfronny.respackopts.util.MetaCache;
import net.fabricmc.api.EnvType;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.resource.*;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.*;

@Mixin(ResourcePackManager.class)
public class ResourcePackManagerMixin {
    @Shadow private Map<String, ResourcePackProfile> profiles;
    @Unique private final Set<CacheKey> rpo$cache = new HashSet<>(); // Tracked here since there might be multiple pack managers (e.g. resource and data)

    @Inject(at = @At("TAIL"), method = "scanPacks()V")
    private void scanPacks(CallbackInfo info) {
        RespackoptsConfig.scanState = RespackoptsConfig.ScanState.SCANNING;
        List<DiscoveredPack> discoveredPacks = new ArrayList<>();
        profiles.forEach((s, v) -> {
            try (ResourcePack rpi = v.createResourcePack()) {
                DiscoveredPack dp = rpo$checkProfile(s, v.getDisplayName().getString(), rpi);
                if (dp != null) discoveredPacks.add(dp);
            }
        });
        Set<CacheKey> newKeys = MetaCache.addFromScan(discoveredPacks, rpo$cache);
        rpo$cache.clear();
        rpo$cache.addAll(newKeys);
        RespackoptsConfig.scanState = RespackoptsConfig.ScanState.DONE;
    }

    @Unique
    private static @Nullable DiscoveredPack rpo$checkProfile(String profileName, String displayName, ResourcePack rpi) {
        Path dataLocation = null;
        if (rpi instanceof DirectoryResourcePack drp) {
            Path pack = ((DirectoryResourcePackAccessor) drp).getRoot();
            Path parent = pack == null ? null : pack.getParent();
            if (parent != null) dataLocation = parent.resolve(pack.getFileName() + Respackopts.FILE_EXTENSION);
            else Respackopts.LOGGER.warn("Base path of directory resource pack {0} is null. This shouldn't happen!", rpi.getId());
        } else if (rpi instanceof ZipResourcePack zrp) {
            File pack = ((ZipFileWrapperAccessor) ((ZipResourcePackAccessor) zrp).getZipFile()).getFile();
            Path parent = pack == null ? null : pack.toPath().getParent();
            if (parent != null) dataLocation = parent.resolve(pack.getName() + Respackopts.FILE_EXTENSION);
            else Respackopts.LOGGER.warn("Base path of zip resource pack {0} is null. This shouldn't happen!", rpi.getId());
        }

        var conf = rpi.openRoot(Respackopts.ID + ".json5");
        if (conf != null) {
            try (InputStream is = conf.get()) {
                return rpo$readConfiguration(rpi, is, dataLocation, rpi.getId(), displayName);
            } catch (Throwable e) {
                String message = "Could not read respackopts config in root for " + profileName;
                if (RespackoptsConfig.debugLogs) Respackopts.LOGGER.error(message, e);
                else Respackopts.LOGGER.error(message);
            }
        }

        Identifier confId = Identifier.of(Respackopts.ID, "conf.json");
        for (ResourceType type : ResourceType.values()) {
            conf = rpi.open(type, confId);
            if (conf != null) {
                try (InputStream is = conf.get()) {
                    return rpo$readConfiguration(rpi, is, dataLocation, rpi.getId(), displayName);
                } catch (Throwable e) {
                    Respackopts.LOGGER.error("Could not initialize pack meta for " + profileName, e);
                }
            }
        }

        return null;
    }

    @Unique
    private static @Nullable DiscoveredPack rpo$readConfiguration(ResourcePack pack, InputStream is, Path dataLocation, String packName, String displayName) throws IOException {
        try (InputStreamReader isr = new InputStreamReader(is)) {
            PackMeta conf = GC_PackMeta.deserialize(isr, LibJf.LENIENT_TRANSPORT);
            if (!Respackopts.isLegal(conf.id)) {
                if (conf.version >= 10) {
                    Respackopts.LOGGER.error("{0} was not loaded as it uses an unsupported pack id: {1}", displayName, conf.id);
                    return null;
                } else conf.id = Respackopts.sanitizeString(conf.id);
            }
            if (RespackoptsConfig.debugLogs) Respackopts.LOGGER.info("Discovered pack: {0}", conf.id);
            if (Respackopts.META_VERSION < conf.version) {
                Respackopts.LOGGER.error("{0} was not loaded as it specifies a newer respackopts version than is installed", displayName);
                return null;
            }
            if (conf.version >= 13) {
                conf.conf.setPresets(rpo$findPresets(pack));
            }
            if (dataLocation == null) dataLocation = Respackopts.FALLBACK_CONF_DIR.resolve(conf.id + ".json");
            return new DiscoveredPack(displayName, packName, conf, dataLocation, FallbackI18n.loadFrom(pack, conf.id));
        }
    }

    @Unique
    private static Map<String, InputSupplier<InputStream>> rpo$findPresets(ResourcePack pack) throws IOException {
        Map<String, InputSupplier<InputStream>> presets = new HashMap<>();
        for (ResourceType type : ResourceType.values()) {
            if (type == ResourceType.CLIENT_RESOURCES && FabricLoader.getInstance().getEnvironmentType() == EnvType.SERVER) continue;
            pack.findResources(type, Respackopts.ID, "presets", (s, r) -> {
                String path = s.getPath();
                if (path.endsWith(".json5")) {
                    presets.put("preset." + path.substring(8, path.length() - 6), r);
                }
            });
        }
        return presets;
    }
}
