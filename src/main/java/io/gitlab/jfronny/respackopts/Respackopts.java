package io.gitlab.jfronny.respackopts;

import io.gitlab.jfronny.commons.logger.SystemLoggerPlus;
import io.gitlab.jfronny.respackopts.integration.SaveHook;
import io.gitlab.jfronny.respackopts.server.ServerInstanceHolder;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Pattern;

public class Respackopts implements ModInitializer, SaveHook {
    public static final Integer META_VERSION = 13;
    public static final String FILE_EXTENSION = ".rpo";

    public static final String ID = "respackopts";
    public static final SystemLoggerPlus LOGGER = SystemLoggerPlus.forName(ID);

    public static final Path FALLBACK_CONF_DIR = FabricLoader.getInstance().getConfigDir().resolve(ID);

    static {
        try {
            Files.createDirectories(FALLBACK_CONF_DIR);
        } catch (IOException e) {
            LOGGER.error("Could not initialize config directory", e);
        }
    }

    @Override
    public void onInitialize() {
        ServerInstanceHolder.init();
    }

    @Override
    public CompletableFuture<Void> onSave(Arguments args) {
        JFC_RespackoptsConfig.INSTANCE.write();

        if (args.reloadData() && FabricLoader.getInstance().getEnvironmentType() == EnvType.SERVER) {
            ServerInstanceHolder.reloadResources();
        }

        return CompletableFuture.completedFuture(null);
    }

    // ^ = start of string
    // $ = end of string
    // * = zero or more times
    // [\\s_] = whitespace or underscores
    // | = or
    // [^a-zA-Z_] = not character or underscore
    private static final Pattern UNSUPPORTED = Pattern.compile("\\W+|^\\d+");
    private static final Pattern SANITIZABLE = Pattern.compile(UNSUPPORTED + "|^_+|_+$");

    public static String sanitizeString(String s) {
        // This trims whitespace/underscores and removes non-alphabetical or underscore characters
        return SANITIZABLE.matcher(s).replaceAll("");
    }

    public static boolean isLegal(String s) {
        return !UNSUPPORTED.matcher(s).find();
    }
}
