package io.gitlab.jfronny.respackopts.integration;

import java.util.concurrent.CompletableFuture;

public interface SaveHook {
    CompletableFuture<Void> onSave(Arguments args);

    record Arguments(boolean flagResourcesForReload, boolean reloadResourcesImmediately, boolean reloadData) {
        public static final Arguments DO_NOTHING = new Arguments(false, false, false);
        public static final Arguments RELOAD_ALL = new Arguments(true, true, true);
    }

    interface Client extends SaveHook {
    }
}
