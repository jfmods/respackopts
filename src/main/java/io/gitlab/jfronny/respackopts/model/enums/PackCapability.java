package io.gitlab.jfronny.respackopts.model.enums;

public enum PackCapability {
    FileFilter, DirFilter, DirFilterAdditive
}
