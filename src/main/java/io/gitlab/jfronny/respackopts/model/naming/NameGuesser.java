package io.gitlab.jfronny.respackopts.model.naming;

import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class NameGuesser {
    private static final Pattern boundary = Pattern.compile(
            "[\\s_-]" +                                                          // Split on whitespace and underscores
            "|(?<=\\p{javaLowerCase})(?=\\p{javaUpperCase})" +                   // Split between lowercase and uppercase letters
            "|(?<=\\p{javaUpperCase})(?=\\p{javaUpperCase}\\p{javaLowerCase})" + // Split on bound of caps lock
            "|(?<=\\D)(?=\\d)" +                                                 // Split between letters and numbers
            "|(?<=\\d)(?=\\D)"                                                   // Split between numbers and letters
    );

    public static String guess(String id) {
        if (id == null || id.isEmpty()) return id;

        return boundary.splitAsStream(id)
                .filter(Predicate.not(String::isBlank))
                .map(word -> {
                    if (word.equals(word.toUpperCase())) return word;
                    else return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
                })
                .collect(Collectors.joining(" "));
    }
}
