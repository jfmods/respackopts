package io.gitlab.jfronny.respackopts.model;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.respackopts.model.enums.PackCapability;
import io.gitlab.jfronny.respackopts.model.tree.ConfigBranch;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@GSerializable
public class PackMeta {
    public ConfigBranch conf;
    public String id;
    public int version;
    public Set<PackCapability> capabilities = new HashSet<>(Set.of(PackCapability.FileFilter));

    public PackMeta(String id, int version, ConfigBranch conf) {
        this.id = Objects.requireNonNull(id, "id");
        this.version = version;
        this.conf = Objects.requireNonNull(conf, "conf");
    }
}
