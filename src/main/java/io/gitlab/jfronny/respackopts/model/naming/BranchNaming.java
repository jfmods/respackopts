package io.gitlab.jfronny.respackopts.model.naming;

import io.gitlab.jfronny.libjf.config.api.v2.Naming;
import io.gitlab.jfronny.libjf.config.api.v2.type.Type;
import io.gitlab.jfronny.libjf.config.impl.ConfigCore;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.RespackoptsConfig;
import io.gitlab.jfronny.respackopts.model.tree.ConfigBranch;
import net.minecraft.text.Text;
import net.minecraft.util.Language;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class BranchNaming implements Naming {
    private final String name;
    private final String path;
    private final int version;

    public BranchNaming(String packId, ConfigBranch cb) {
        this(packId, packId, cb.getVersion());
    }

    public BranchNaming(String packId) {
        this(packId, packId, Respackopts.META_VERSION);
    }

    private BranchNaming(String name, String path, int version) {
        this.name = name;
        this.path = path;
        this.version = version;
    }

    private static Text maybeFallback(String key, String fallback) {
        if (!RespackoptsConfig.generateNames) return Text.translatable(key);
        return Text.translatableWithFallback(key, NameGuesser.guess(fallback));
    }

    private static @Nullable Text maybeText(String key) {
        return Language.getInstance().hasTranslation(key) ? Text.translatable(key) : null;
    }

    @Override
    public Text name() {
        return version < 3
                ? maybeFallback("respackopts.title." + path, name)
                : maybeFallback("rpo." + path, name);
    }

    @Override
    public @Nullable Text description() {
        return version < 3
                ? maybeText("respackopts.tooltip." + path)
                : version < 9
                    ? maybeText("rpo.tooltip." + path)
                    : maybeText("rpo." + path + ".tooltip");
    }

    @Override
    public Text preset(String name) {
        return maybeFallback("rpo." + path + "." + name, name);
    }

    @Override
    public Naming category(String name) {
        return new BranchNaming(name, path + "." + name, version);
    }

    @Override
    public Entry entry(String name) {
        return new Entry() {
            private String fieldPath() {
                return version < 3
                        ? "respackopts.field." + path + "." + name
                        : "rpo." + path + "." + name;
            }

            @Override
            public Text name() {
                return maybeFallback(fieldPath(), name);
            }

            @Override
            public @Nullable Text tooltip() {
                return version < 3
                        ? maybeText("respackopts.tooltip." + path + "." + name)
                        : version < 9
                            ? maybeText("rpo.tooltip." + path + "." + name)
                            : maybeText("rpo." + path + "." + name + ".tooltip");
            }

            @Override
            public Text boolValue(boolean value) {
                String bs = Boolean.toString(value);
                String path = fieldPath() + "." + value;
                return Language.getInstance().hasTranslation(path)
                        ? Text.translatableWithFallback(path, bs)
                        : Text.translatableWithFallback(ConfigCore.MOD_ID + "." + value, bs);
            }

            @Override
            public Text enumValue(Type type, Object value) {
                String bs = Objects.toString(value);
                return maybeFallback(fieldPath() + "." + bs, bs);
            }

            @Override
            public Text nullValue() {
                return Text.translatableWithFallback(ConfigCore.MOD_ID + ".null", "null");
            }
        };
    }
}
