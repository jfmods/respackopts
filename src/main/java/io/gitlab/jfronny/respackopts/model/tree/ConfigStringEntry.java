package io.gitlab.jfronny.respackopts.model.tree;

import io.gitlab.jfronny.commons.serialize.databind.api.SerializeWithAdapter;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.CategoryBuilder;
import io.gitlab.jfronny.muscript.data.dynamic.DString;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.RespackoptsConfig;
import io.gitlab.jfronny.respackopts.serialization.entry.StringEntryTypeAdapter;

import java.util.Objects;

@SerializeWithAdapter(adapter = StringEntryTypeAdapter.class)
public class ConfigStringEntry extends ConfigEntry<String> implements DString {
    public ConfigStringEntry(String v) {
        super(String.class);
        setValue(v);
        setDefault(v);
    }

    @Override
    public ConfigEntry<String> clone() {
        ConfigStringEntry be = new ConfigStringEntry(getValue());
        be.setDefault(getDefault());
        return be;
    }

    @Override
    public void buildShader(StringBuilder sb, String valueName) {
    }

    @Override
    public CategoryBuilder<?> buildEntry(GuiEntryBuilderParam args) {
        return args.builder().value(args.name(), getDefault(), this::getValue, v -> {
            if (!Objects.equals(getValue(), v)) {
                if (RespackoptsConfig.debugLogs) Respackopts.LOGGER.info("ConfigStringEntry SaveCallback");
                args.saveCallback();
            }
            setValue(v);
        });
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o) && o instanceof ConfigStringEntry cb && getValue().equals(cb.getValue()) && getDefault().equals(cb.getDefault());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getValue(), getDefault());
    }
}
