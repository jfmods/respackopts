package io.gitlab.jfronny.respackopts.model.tree;

import io.gitlab.jfronny.commons.serialize.databind.api.SerializeWithAdapter;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.CategoryBuilder;
import io.gitlab.jfronny.muscript.data.dynamic.DNumber;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.RespackoptsConfig;
import io.gitlab.jfronny.respackopts.serialization.entry.NumericEntryTypeAdapter;
import io.gitlab.jfronny.respackopts.model.enums.ConfigSyncMode;
import io.gitlab.jfronny.respackopts.util.IndentingStringBuilder;

import java.util.Objects;

@SerializeWithAdapter(adapter = NumericEntryTypeAdapter.class)
public class ConfigNumericEntry extends ConfigEntry<Double> implements DNumber {
    private Double min = null;
    private Double max = null;
    private boolean integer = false;
    
    public ConfigNumericEntry() {
        super(Double.class);
        setValue(0d);
    }

    public ConfigNumericEntry asInteger() {
        this.integer = true;
        if (min != null && (min % 1.0 != 0)) {
            this.integer = false;
            throw new RuntimeException("Minimum value (" + min + ") is not integer for integer entry");
        }
        if (max != null && (max % 1.0 != 0)) {
            this.integer = false;
            throw new RuntimeException("Minimum value (" + max + ") is not integer for integer entry");
        }
        return this;
    }

    public void setMin(Double value) {
        if (value != null && integer && (value % 1.0 != 0))
            throw new RuntimeException("Minimum value (" + value + ") is not integer for integer entry");
        this.min = value;
    }

    public void setMax(Double value) {
        if (value != null && integer && (value % 1.0 != 0))
            throw new RuntimeException("Maximum value (" + value + ") is not integer for integer entry");
        this.max = value;
    }

    @Override
    public Double setDefault(Double value) {
        if (integer && (value % 1.0 != 0))
            throw new RuntimeException("Default value (" + value + ") is not integer for integer entry");
        if ((min != null && value < min) || (max != null && value > max))
            throw new RuntimeException("Default value (" + value + ") out of range in number entry definition (" + min + "-" + max + ")");
        return super.setDefault(value);
    }

    @Override
    public void sync(ConfigEntry<Double> source, ConfigSyncMode mode) {
        super.sync(source, mode);
        ConfigNumericEntry n = (ConfigNumericEntry) source;
        if (mode == ConfigSyncMode.RESPACK_LOAD) {
            min = n.min;
            max = n.max;
            integer = n.integer;
        }
    }

    @Override
    public void appendString(IndentingStringBuilder sb) {
        sb.line(getValue() + " (" + getDefault() + ", " + min + "-" + max + (integer ? ", integer" : "") + ")");
    }

    @Override
    public ConfigEntry<Double> clone() {
        ConfigNumericEntry ce = new ConfigNumericEntry();
        ce.setMax(max);
        ce.setMin(min);
        ce.setValue(getValue());
        ce.setDefault(getDefault());
        return integer ? ce.asInteger() : ce;
    }

    @Override
    public void buildShader(StringBuilder sb, String valueName) {
        sb.append("\n#define ");
        sb.append(valueName);
        sb.append(' ');
        sb.append(getValue().toString());
    }

    @Override
    public CategoryBuilder<?> buildEntry(GuiEntryBuilderParam args) {
        double min = this.min == null ? Double.NEGATIVE_INFINITY : this.min;
        double max = this.max == null ? Double.POSITIVE_INFINITY : this.max;
        if (integer) {
            return args.builder().value(
                    args.name(),
                    asLong(getDefault()),
                    min,
                    max,
                    () -> asLong(getValue()),
                    v -> {
                        if (!Objects.equals(asLong(getValue()), v)) {
                            if (RespackoptsConfig.debugLogs) Respackopts.LOGGER.info("ConfigNumericEntryNormal SaveCallback");
                            args.saveCallback();
                        }
                        setValue(v == null ? null : v.doubleValue());
                    }
            );
        } else {
            return args.builder().value(
                    args.name(),
                    getDefault(),
                    min,
                    max,
                    this::getValue,
                    v -> {
                        if (!Objects.equals(getValue(), v)) {
                            if (RespackoptsConfig.debugLogs) Respackopts.LOGGER.info("ConfigNumericEntryNormal SaveCallback");
                            args.saveCallback();
                        }
                        setValue(v);
                    }
            );
        }
    }

    private static Long asLong(Double d) {
        return d == null ? null : d.longValue();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConfigNumericEntry that)) return false;
        if (!super.equals(o)) return false;
        return Objects.equals(getValue(), that.getValue())
                && Objects.equals(getDefault(), that.getDefault())
                && Objects.equals(min, that.min)
                && Objects.equals(max, that.max)
                && integer == that.integer;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getValue(), getDefault(), min, max, integer);
    }
}
