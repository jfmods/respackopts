package io.gitlab.jfronny.respackopts.model.tree;

import io.gitlab.jfronny.libjf.config.api.v2.dsl.CategoryBuilder;

public record GuiEntryBuilderParam(CategoryBuilder<?> builder, String name, Runnable onSave) {
    public void saveCallback() {
        onSave.run();
    }
}