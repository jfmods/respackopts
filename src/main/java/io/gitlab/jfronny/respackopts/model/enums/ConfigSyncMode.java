package io.gitlab.jfronny.respackopts.model.enums;

public enum ConfigSyncMode {
    RESPACK_LOAD, CONF_LOAD
}
