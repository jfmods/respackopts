package io.gitlab.jfronny.respackopts.model.naming;

import io.gitlab.jfronny.libjf.config.api.v2.ConfigInstance;
import io.gitlab.jfronny.libjf.config.api.v2.Naming;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.model.cache.CacheKey;
import io.gitlab.jfronny.respackopts.util.MetaCache;

public class RespackoptsNaming extends Naming.Delegate implements Naming.Custom {
    public RespackoptsNaming() {
        super(Respackopts.ID);
    }

    @Override
    public String getId() {
        return Respackopts.ID;
    }

    @Override
    public Naming referenced(ConfigInstance reference) {
        String packId = reference.getId();
        CacheKey ck = MetaCache.getKeyByPackId(packId);
        return ck == null ? new BranchNaming(packId) : new BranchNaming(packId, MetaCache.getBranch(ck));
    }
}
