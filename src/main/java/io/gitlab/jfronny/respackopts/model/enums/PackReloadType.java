package io.gitlab.jfronny.respackopts.model.enums;

import java.util.function.Consumer;

public enum PackReloadType {
    Resource, Simple;

    public static class Aggregator implements Consumer<PackReloadType> {
        private PackReloadType reloadType = Simple;

        @Override
        public void accept(PackReloadType packReloadType) {
            if (packReloadType == PackReloadType.Resource) {
                reloadType = PackReloadType.Resource;
            }
        }

        public PackReloadType get() {
            return reloadType;
        }
    }
}
