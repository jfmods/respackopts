package io.gitlab.jfronny.respackopts.model;

import java.nio.file.Path;
import java.util.Map;

public record DiscoveredPack(String displayName, String packName, PackMeta meta, Path dataLocation, Map<String, String> translations) {
}
