package io.gitlab.jfronny.respackopts.model.cache;

import io.gitlab.jfronny.muscript.ast.context.Script;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;
import io.gitlab.jfronny.respackopts.RespackoptsConfig;
import io.gitlab.jfronny.respackopts.filters.util.FileDependencyTracker;
import io.gitlab.jfronny.respackopts.model.DirRpo;
import io.gitlab.jfronny.respackopts.model.FileRpo;
import io.gitlab.jfronny.respackopts.model.PackMeta;
import io.gitlab.jfronny.respackopts.model.tree.ConfigBranch;
import io.gitlab.jfronny.respackopts.muscript.MuScriptScope;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public record CachedPackState(
        String packId,
        String displayName,
        String packName,
        ConfigBranch configBranch,
        PackMeta metadata,
        Map<String, FileRpo> cachedFileRPOs,
        Map<String, List<DirRpo>> cachedDirRPOs, // Directory RPOs, from outermost to innermost
        Map<String, Script> cachedScripts, // Scripts, available via runScript
        Map<String, String> cachedFiles, // Files,  read by readString
        Scope executionScope,
        @Nullable FileDependencyTracker tracker
) {
    public CachedPackState(CacheKey key, PackMeta meta, ConfigBranch branch) {
        this(
                meta.id,
                key.displayName(),
                key.packName(),
                branch,
                meta,
                new HashMap<>(),
                new HashMap<>(),
                new HashMap<>(),
                new HashMap<>(),
                branch.addTo(MuScriptScope.fork(meta.version)),
                RespackoptsConfig.debugLogs ? new FileDependencyTracker(key.displayName()) : null
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CachedPackState that)) return false;
        return Objects.equals(packId, that.packId)
                && Objects.equals(displayName, that.displayName)
                && Objects.equals(packName, that.packName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(packId, displayName, packName);
    }
}
