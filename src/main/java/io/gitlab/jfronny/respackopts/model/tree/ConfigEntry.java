package io.gitlab.jfronny.respackopts.model.tree;

import io.gitlab.jfronny.commons.serialize.databind.api.SerializeWithAdapter;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.CategoryBuilder;
import io.gitlab.jfronny.muscript.data.dynamic.DynamicBase;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.serialization.ConfigEntryTypeAdapter;
import io.gitlab.jfronny.respackopts.model.enums.ConfigSyncMode;
import io.gitlab.jfronny.respackopts.model.enums.PackReloadType;
import io.gitlab.jfronny.respackopts.util.IndentingStringBuilder;

import java.util.Objects;

@SerializeWithAdapter(adapter = ConfigEntryTypeAdapter.class)
public abstract class ConfigEntry<T> implements DynamicBase {
    private final Class<? super T> entryClass;
    private T defaultValue;
    private T value;
    private PackReloadType reloadType = PackReloadType.Resource;
    protected int version;
    protected ConfigBranch parent;

    public ConfigEntry(Class<? super T> entryClass) {
        this.entryClass = entryClass;
    }

    public String getName() {
        if (parent == null)
            return "";
        String n = parent.getName() + "." + parent.getEntryName(this);
        if (n.startsWith("."))
            n = n.substring(1);
        return n;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    public int getVersion() {
        return version;
    }
    
    public T getValue() {
        if (value == null) {
            if (defaultValue == null) {
                Respackopts.LOGGER.warn("No default value or current value set for entry, returning null in {0}", getName());
                return null;
            }
            value = getDefault();
        }
        return value;
    }
    
    public T setValue(T value) {
        if (value != null)
            this.value = value;
        return this.value;
    }

    public T getDefault() {
        if (defaultValue == null) {
            defaultValue = getValue();
            Respackopts.LOGGER.warn("No default value set for entry, using current in {0}", getName());
        }
        return defaultValue;
    }
    
    public T setDefault(T value) {
        if (value != null)
            defaultValue = value;
        return defaultValue;
    }

    public boolean hasDefault() {
        return defaultValue != null;
    }
    
    public void sync(ConfigEntry<T> source, ConfigSyncMode mode) {
        if (mode == ConfigSyncMode.RESPACK_LOAD)
            setDefault(source.getDefault());
        if (mode == ConfigSyncMode.CONF_LOAD)
            setValue(source.getValue());
    }
    
    public void appendString(IndentingStringBuilder sb) {
        sb.line(getValue() + " (" + getDefault() + ")");
    }

    @Override
    public String toString() {
        IndentingStringBuilder log = new IndentingStringBuilder();
        appendString(log);
        return log.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConfigEntry<?> that)) return false;
        return version == that.version && Objects.equals(entryClass, that.entryClass) && reloadType == that.reloadType && (parent == null) == (that.parent == null);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entryClass, reloadType, version, parent == null);
    }

    public abstract ConfigEntry<T> clone();

    public abstract void buildShader(StringBuilder sb, String valueName);

    public abstract CategoryBuilder<?> buildEntry(GuiEntryBuilderParam args);

    public Class<? super T> getEntryClass() {
        return entryClass;
    }

    public PackReloadType getReloadType() {
        return reloadType;
    }

    public void setReloadType(PackReloadType reloadType) {
        this.reloadType = reloadType;
    }

    public ConfigBranch getParent() {
        return parent;
    }
}
