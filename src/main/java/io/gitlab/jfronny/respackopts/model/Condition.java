package io.gitlab.jfronny.respackopts.model;

import io.gitlab.jfronny.commons.serialize.databind.api.SerializeWithAdapter;
import io.gitlab.jfronny.muscript.ast.BoolExpr;
import io.gitlab.jfronny.muscript.core.CodeLocation;
import io.gitlab.jfronny.muscript.core.LocationalException;
import io.gitlab.jfronny.muscript.core.PrettyPrintError;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;
import io.gitlab.jfronny.muscript.runtime.Runtime;
import io.gitlab.jfronny.muscript.serialize.Decompiler;
import io.gitlab.jfronny.respackopts.serialization.ConditionTypeAdapter;
import org.jetbrains.annotations.Nullable;

@SerializeWithAdapter(adapter = ConditionTypeAdapter.class)
public record Condition(String source, String sourceFile, BoolExpr expr) {
    public boolean get(Scope dataRoot) throws ConditionException {
        try {
            return Runtime.evaluate(expr, dataRoot);
        } catch (LocationalException e) {
            throw new ConditionException(
                    e.asPrintable(),
                    "Your expression was optimized to: " + Decompiler.decompile(expr) + "\nRemember: You can use '/rpoc dump scope' to view your execution scope",
                    e
            );
        } catch (RuntimeException e) {
            throw new ConditionException(
                    PrettyPrintError.builder(new CodeLocation(0, 0, source, sourceFile)).setMessage(e.getMessage()).build(),
                    "Your expression was optimized to: " + Decompiler.decompile(expr) + "\nRemember: You can use '/rpoc dump scope' to view your execution scope",
                    e
            );
        }
    }

    public Condition withSourceFile(String sourceFile) {
        return new Condition(source, sourceFile, expr);
    }

    public static class ConditionException extends Exception {
        public final PrettyPrintError error;

        public ConditionException(PrettyPrintError error, Throwable cause) {
            this(error, null, cause);
        }

        public ConditionException(PrettyPrintError error, @Nullable String messageSuffix, Throwable cause) {
            super(error.toString() + (messageSuffix == null ? "" : "\n" + messageSuffix), cause);
            this.error = error;
        }
    }
}
