package io.gitlab.jfronny.respackopts.model.cache;

import java.nio.file.Path;

public record CacheKey(String displayName, String packName, Path dataLocation) {
    @Override
    public String toString() {
        return "CacheKey{displayName='" + displayName + "', packName='" + packName + "', dataLocation=" + dataLocation + '}';
    }
}
