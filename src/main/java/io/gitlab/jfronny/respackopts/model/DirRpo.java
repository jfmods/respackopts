package io.gitlab.jfronny.respackopts.model;

import io.gitlab.jfronny.commons.serialize.annotations.Ignore;
import io.gitlab.jfronny.commons.serialize.annotations.SerializedName;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

@GSerializable
public class DirRpo {
    @SerializedName(value = "condition", alternate = {"conditions"})
    public Condition condition;
    @SerializedName(value = "fallback", alternate = {"fallbacks"})
    public String fallback;

    @Ignore
    public String path;

    public void hydrate(String path) {
        this.path = path;
        if (condition != null) condition = condition.withSourceFile(path);
    }
}
