package io.gitlab.jfronny.respackopts.model;

import io.gitlab.jfronny.commons.serialize.annotations.Ignore;
import io.gitlab.jfronny.commons.serialize.annotations.SerializedName;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.muscript.ast.StringExpr;

import java.util.Map;
import java.util.Set;

@GSerializable
public class FileRpo {
    @SerializedName(value = "condition", alternate = {"conditions"})
    public Condition condition;
    @SerializedName(value = "fallback", alternate = {"fallbacks"})
    public Set<String> fallbacks;
    @SerializedName(value = "expansion", alternate = {"expansions"})
    public Map<String, StringExpr> expansions;

    @Ignore
    public String path;

    public void hydrate(String path) {
        this.path = path;
        if (condition != null) condition = condition.withSourceFile(path);
    }
}
