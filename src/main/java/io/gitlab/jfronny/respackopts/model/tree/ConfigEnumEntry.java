package io.gitlab.jfronny.respackopts.model.tree;

import io.gitlab.jfronny.commons.serialize.databind.api.SerializeWithAdapter;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.CategoryBuilder;
import io.gitlab.jfronny.muscript.data.additional.DEnum;
import io.gitlab.jfronny.muscript.data.additional.DelegateDynamic;
import io.gitlab.jfronny.muscript.data.dynamic.Dynamic;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.RespackoptsConfig;
import io.gitlab.jfronny.respackopts.serialization.entry.EnumEntryTypeAdapter;
import io.gitlab.jfronny.respackopts.model.enums.ConfigSyncMode;
import io.gitlab.jfronny.respackopts.util.IndentingStringBuilder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

@SerializeWithAdapter(adapter = EnumEntryTypeAdapter.class)
public class ConfigEnumEntry extends ConfigEntry<String> implements DelegateDynamic {
    private final List<String> values = new ArrayList<>();
    private Integer nextValue;

    public ConfigEnumEntry() {
        super(String.class);
    }

    public void setNextValue(int v) {
        nextValue = v;
    }

    @Override
    public String getValue() {
        String v = super.getValue();
        if (v == null) {
            if (nextValue != null && nextValue >= 0 && nextValue < values.size()) {
                v = values.get(nextValue);
                setValue(v);
            }
            else {
                throw new NullPointerException("Could not get value in " + getName());
            }
        }
        return v;
    }

    @Override
    public String getDefault() {
        String v = super.getDefault();
        if (v == null) {
            if (values.isEmpty()) throw new NullPointerException("Could not get default entry as the entry array is empty in " + getName());
            v = values.get(0);
        }
        return v;
    }

    public List<String> getValues() {
        return List.copyOf(values);
    }

    public void setValues(List<String> replacement) {
        values.clear();
        values.addAll(replacement);
        checkValues();
    }

    @Override
    public void setVersion(int version) {
        super.setVersion(version);
        checkValues();
    }

    private void checkValues() {
        if (version < 10) return;
        for (Iterator<String> iterator = values.iterator(); iterator.hasNext();) {
            String value = iterator.next();
            if (!Respackopts.isLegal(value)) {
                Respackopts.LOGGER.error("Illegal enum entry for {0}, skipping: {1}", getName(), value);
                iterator.remove();
            }
        }
    }

    @Override
    public void sync(ConfigEntry<String> source, ConfigSyncMode mode) {
        super.sync(source, mode);

        ConfigEnumEntry n = (ConfigEnumEntry) source;
        if (mode == ConfigSyncMode.RESPACK_LOAD && !n.values.isEmpty()) {
            setValues(n.values);
        }
        if (nextValue != null) {
            if (getValue() == null) {
                if (n.nextValue >= 0 && n.nextValue < values.size()) {
                    setValue(values.get(n.nextValue));
                }
                else {
                    Respackopts.LOGGER.error("Could not load default value for enum in {0}", getName());
                }
            } else nextValue = null;
        }
    }

    @Override
    public void appendString(IndentingStringBuilder sb) {
        sb.line(getValue() + " (default=" + getDefault() + ") of:");
        IndentingStringBuilder isb = sb.indent();
        for (String e : values) {
            isb.line("- " + e);
        }
    }

    @Override
    public ConfigEntry<String> clone() {
        ConfigEnumEntry e = new ConfigEnumEntry();
        e.nextValue = nextValue;
        e.values.addAll(values);
        e.setValue(getValue());
        e.setDefault(getDefault());
        return e;
    }

    @Override
    public void buildShader(StringBuilder sb, String valueName) {
        sb.append("\n#define ");
        sb.append(valueName);
        sb.append(' ');
        sb.append(values.indexOf(getValue()));
        for (int i = 0; i < values.size(); i++) {
            String e2 = Respackopts.sanitizeString(values.get(i));
            if (version == 1) {
                sb.append("\n#define ");
                sb.append(valueName);
                sb.append('_');
                sb.append(e2);
                sb.append(' ');
                sb.append(i);
            } else {
                if (e2.equals(getValue())) {
                    sb.append("\n#define ");
                    sb.append(valueName);
                    sb.append('_');
                    sb.append(e2);
                }
            }
        }
    }

    @Override
    public Dynamic getDelegate() {
        return new DEnum(values, getValue());
    }

    @Override
    public CategoryBuilder<?> buildEntry(GuiEntryBuilderParam args) {
        return args.builder().value(args.name(), getDefault(), values.toArray(String[]::new), this::getValue, v -> {
            if (!Objects.equals(getValue(), v)) {
                if (RespackoptsConfig.debugLogs) Respackopts.LOGGER.info("ConfigEnumEntry SaveCallback");
                args.saveCallback();
            }
            setValue(v);
        });
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConfigEnumEntry that)) return false;
        if (!super.equals(o)) return false;
        return Objects.equals(getValue(), that.getValue()) && Objects.equals(getDefault(), that.getDefault()) && Objects.equals(values, that.values) && Objects.equals(nextValue, that.nextValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getValue(), getDefault(), values, nextValue);
    }
}
