package io.gitlab.jfronny.respackopts.model.tree;

import io.gitlab.jfronny.commons.serialize.databind.api.SerializeWithAdapter;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.CategoryBuilder;
import io.gitlab.jfronny.muscript.data.dynamic.DBool;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.RespackoptsConfig;
import io.gitlab.jfronny.respackopts.serialization.entry.BooleanEntryTypeAdapter;

import java.util.Objects;

@SerializeWithAdapter(adapter = BooleanEntryTypeAdapter.class)
public class ConfigBooleanEntry extends ConfigEntry<Boolean> implements DBool {
    public ConfigBooleanEntry(boolean v) {
        super(Boolean.class);
        setValue(v);
        setDefault(v);
    }

    @Override
    public ConfigEntry<Boolean> clone() {
        ConfigBooleanEntry be = new ConfigBooleanEntry(getValue());
        be.setDefault(getDefault());
        return be;
    }

    @Override
    public void buildShader(StringBuilder sb, String valueName) {
        if (getValue()) {
            sb.append("\n#define ");
            sb.append(valueName);
        }
    }

    @Override
    public CategoryBuilder<?> buildEntry(GuiEntryBuilderParam args) {
        return args.builder().value(args.name(), getDefault(), this::getValue, v -> {
            if (getValue() != v) {
                if (RespackoptsConfig.debugLogs) Respackopts.LOGGER.info("ConfigBooleanEntry SaveCallback");
                args.saveCallback();
            }
            setValue(v);
        });
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o) && o instanceof ConfigBooleanEntry cb && getValue().equals(cb.getValue()) && getDefault().equals(cb.getDefault());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getValue(), getDefault());
    }
}
