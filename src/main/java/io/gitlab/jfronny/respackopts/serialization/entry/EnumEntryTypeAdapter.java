package io.gitlab.jfronny.respackopts.serialization.entry;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.Token;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.respackopts.model.enums.PackReloadType;
import io.gitlab.jfronny.respackopts.model.tree.ConfigEnumEntry;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@SerializerFor(targets = ConfigEnumEntry.class)
public class EnumEntryTypeAdapter extends TypeAdapter<ConfigEnumEntry> {
    public static final Set<String> TYPES = Set.of("enum", "select");

    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(ConfigEnumEntry configEnumEntry, Writer writer) throws TEx, MalformedDataException {
        writer.value(configEnumEntry.getValue());
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> ConfigEnumEntry deserialize(Reader reader) throws TEx, MalformedDataException {
        ConfigEnumEntry result = new ConfigEnumEntry();
        if (reader.peek() == Token.STRING) {
            result.setValue(reader.nextString());
            return result;
        } else if (reader.peek() == Token.NUMBER) {
            result.setNextValue(reader.nextInt());
            return result;
        } else if (reader.peek() == Token.BEGIN_ARRAY) {
            List<String> replacement = new ArrayList<>();
            reader.beginArray();
            while (reader.hasNext()) {
                if (reader.peek() != Token.STRING) throw new MalformedDataException("Expected string entry in enum");
                replacement.add(reader.nextString());
            }
            reader.endArray();
            result.setValues(replacement);
            result.setDefault(replacement.getFirst());
            result.setValue(replacement.getFirst());
            return result;
        } else if (reader.peek() == Token.BEGIN_OBJECT) {
            reader.beginObject();
            while (reader.hasNext()) {
                String key = reader.nextName();
                switch (key) {
                    case "type" -> {
                        if (!TYPES.contains(reader.nextString())) {
                            throw new MalformedDataException("Invalid type for enum entry");
                        }
                    }
                    case "default" -> {
                        if (reader.peek() != Token.STRING) throw new MalformedDataException("Expected string for enum default");
                        String value = reader.nextString();
                        result.setDefault(value);
                        result.setValue(value);
                    }
                    case "reloadType" -> result.setReloadType(PackReloadType.valueOf(reader.nextString()));
                    case "values" -> {
                        if (reader.peek() != Token.BEGIN_ARRAY) throw new MalformedDataException("Expected array for enum values");
                        List<String> replacement = new ArrayList<>();
                        reader.beginArray();
                        while (reader.hasNext()) {
                            if (reader.peek() != Token.STRING) throw new MalformedDataException("Expected string entry in enum");
                            replacement.add(reader.nextString());
                        }
                        reader.endArray();
                        result.setValues(replacement);
                        if (!result.hasDefault()) {
                            result.setDefault(replacement.getFirst());
                            result.setValue(replacement.getFirst());
                        }
                    }
                    default -> throw new MalformedDataException("Unknown key in enum entry: " + key);
                }
            }
            reader.endObject();
            return result;
        } else {
            throw new MalformedDataException("Invalid data type for enum entry");
        }
    }
}
