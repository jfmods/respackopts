package io.gitlab.jfronny.respackopts.serialization;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.respackopts.model.Condition;

@SerializerFor(targets = Condition.class)
public class ConditionTypeAdapter extends TypeAdapter<Condition> {
    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(Condition condition, Writer writer) throws TEx, MalformedDataException {
        BoolExprTypeAdapter.INSTANCE.serialize(condition.expr(), writer);
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> Condition deserialize(Reader reader) throws TEx, MalformedDataException {
        return new Condition("Source unavailable", null, BoolExprTypeAdapter.INSTANCE.deserialize(reader));
    }
}
