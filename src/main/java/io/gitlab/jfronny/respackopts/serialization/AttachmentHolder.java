package io.gitlab.jfronny.respackopts.serialization;

import io.gitlab.jfronny.commons.concurrent.ScopedValue;
import io.gitlab.jfronny.commons.throwable.ExceptionWrapper;
import io.gitlab.jfronny.commons.throwable.ThrowingRunnable;
import io.gitlab.jfronny.commons.throwable.ThrowingSupplier;

public class AttachmentHolder {
    private static final ScopedValue<Attachment> attachments = new ScopedValue<>();

    public static <TEx extends Throwable> void attach(int version, ThrowingRunnable<TEx> runnable) throws TEx {
        try {
            ScopedValue.runWhere(attachments, new Attachment(version), runnable.orThrow());
        } catch (ExceptionWrapper e) {
            throw (TEx) ExceptionWrapper.unwrap(e);
        }
    }

    public static <TOut, TEx extends Throwable> TOut attach(int version, ThrowingSupplier<TOut, TEx> runnable) throws TEx {
        try {
            return ScopedValue.getWhere(attachments, new Attachment(version), runnable.orThrow());
        } catch (ExceptionWrapper e) {
            throw (TEx) ExceptionWrapper.unwrap(e);
        }
    }

    public static int getAttachedVersion() {
        return attachments.get().version;
    }

    private record Attachment(int version) {
    }
}
