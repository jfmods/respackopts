package io.gitlab.jfronny.respackopts.serialization.entry;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.Token;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.respackopts.model.enums.PackReloadType;
import io.gitlab.jfronny.respackopts.model.tree.ConfigBooleanEntry;

import java.util.Set;

@SerializerFor(targets = ConfigBooleanEntry.class)
public class BooleanEntryTypeAdapter extends TypeAdapter<ConfigBooleanEntry> {
    public static final Set<String> TYPES = Set.of("boolean", "toggle");

    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(ConfigBooleanEntry configBooleanEntry, Writer writer) throws TEx, MalformedDataException {
        writer.value(configBooleanEntry.getValue());
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> ConfigBooleanEntry deserialize(Reader reader) throws TEx, MalformedDataException {
        if (reader.peek() == Token.BOOLEAN) {
            return new ConfigBooleanEntry(reader.nextBoolean());
        } else if (reader.peek() == Token.BEGIN_OBJECT) {
            reader.beginObject();
            ConfigBooleanEntry result = new ConfigBooleanEntry(false);
            while (reader.hasNext()) {
                String key = reader.nextName();
                switch (key) {
                    case "type" -> {
                        if (!TYPES.contains(reader.nextString())) {
                            throw new MalformedDataException("Invalid type for boolean entry");
                        }
                    }
                    case "default" -> {
                        boolean value = reader.nextBoolean();
                        result.setDefault(value);
                        result.setValue(value);
                    }
                    case "reloadType" -> result.setReloadType(PackReloadType.valueOf(reader.nextString()));
                    default -> throw new MalformedDataException("Unknown key in boolean entry: " + key);
                };
            }
            reader.endObject();
            return result;
        } else {
            throw new MalformedDataException("Invalid data type for boolean entry");
        }
    }
}
