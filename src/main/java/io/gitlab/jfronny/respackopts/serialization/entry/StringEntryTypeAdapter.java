package io.gitlab.jfronny.respackopts.serialization.entry;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.Token;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.respackopts.model.enums.PackReloadType;
import io.gitlab.jfronny.respackopts.model.tree.ConfigBooleanEntry;
import io.gitlab.jfronny.respackopts.model.tree.ConfigStringEntry;

import java.util.Set;

@SerializerFor(targets = ConfigBooleanEntry.class)
public class StringEntryTypeAdapter extends TypeAdapter<ConfigStringEntry> {
    public static final Set<String> TYPES = Set.of("string", "text");

    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(ConfigStringEntry configBooleanEntry, Writer writer) throws TEx, MalformedDataException {
        writer.value(configBooleanEntry.getValue());
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> ConfigStringEntry deserialize(Reader reader) throws TEx, MalformedDataException {
        if (reader.peek() == Token.BEGIN_OBJECT) {
            reader.beginObject();
            ConfigStringEntry result = new ConfigStringEntry("");
            while (reader.hasNext()) {
                String key = reader.nextName();
                switch (key) {
                    case "type" -> {
                        if (!TYPES.contains(reader.nextString())) {
                            throw new MalformedDataException("Invalid type for string entry");
                        }
                    }
                    case "default" -> {
                        String value = reader.nextString();
                        result.setDefault(value);
                        result.setValue(value);
                    }
                    case "reloadType" -> result.setReloadType(PackReloadType.valueOf(reader.nextString()));
                    default -> throw new MalformedDataException("Unknown key in string entry: " + key);
                };
            }
            reader.endObject();
            return result;
        } else {
            throw new MalformedDataException("Invalid data type for string entry");
        }
    }
}
