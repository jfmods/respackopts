package io.gitlab.jfronny.respackopts.serialization;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.muscript.ast.StringExpr;
import io.gitlab.jfronny.muscript.ast.context.ExprUtils;

@SerializerFor(targets = StringExpr.class)
public class StringExprTypeAdapter extends TypeAdapter<StringExpr> {
    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(StringExpr stringExpr, Writer writer) throws TEx, MalformedDataException {
        ExprTypeAdapter.INSTANCE.serialize(stringExpr, writer);
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> StringExpr deserialize(Reader reader) throws TEx, MalformedDataException {
        return ExprUtils.asString(ExprTypeAdapter.INSTANCE.deserialize(reader));
    }
}
