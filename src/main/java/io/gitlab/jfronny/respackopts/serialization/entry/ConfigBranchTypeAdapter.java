package io.gitlab.jfronny.respackopts.serialization.entry;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.Token;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.respackopts.model.tree.ConfigBranch;
import io.gitlab.jfronny.respackopts.model.tree.ConfigEntry;
import io.gitlab.jfronny.respackopts.model.tree.GC_ConfigEntry;

import java.util.Map;

@SerializerFor(targets = ConfigBranch.class)
public class ConfigBranchTypeAdapter extends TypeAdapter<ConfigBranch> {
    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(ConfigBranch configBranch, Writer writer) throws TEx, MalformedDataException {
        writer.beginObject();
        for (Map.Entry<String, ConfigEntry<?>> entry : configBranch.getValue().entrySet()) {
            writer.name(entry.getKey());
            GC_ConfigEntry.serialize(entry.getValue(), writer);
        }
        writer.endObject();
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> ConfigBranch deserialize(Reader reader) throws TEx, MalformedDataException {
        if (reader.peek() != Token.BEGIN_OBJECT) throw new MalformedDataException("ConfigBranch is not an object");
        reader.beginObject();
        ConfigBranch cbNew = new ConfigBranch();
        while (reader.hasNext()) {
            String s = reader.nextName();
            cbNew.add(s, GC_ConfigEntry.deserialize(reader));
        }
        reader.endObject();
        return cbNew;
    }
}
