package io.gitlab.jfronny.respackopts.serialization.entry;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.Token;
import io.gitlab.jfronny.commons.serialize.databind.api.SerializerFor;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.respackopts.model.enums.PackReloadType;
import io.gitlab.jfronny.respackopts.model.tree.ConfigNumericEntry;

import java.util.Set;

@SerializerFor(targets = ConfigNumericEntry.class)
public class NumericEntryTypeAdapter extends TypeAdapter<ConfigNumericEntry> {
    public static final Set<String> TYPES = Set.of("numeric", "number", "slider", "integer", "int", "long", "whole");
    public static final Set<String> INT_TYPES = Set.of("integer", "int", "long", "whole");

    @Override
    public <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(ConfigNumericEntry configNumericEntry, Writer writer) throws TEx, MalformedDataException {
        writer.value(configNumericEntry.getValue());
    }

    @Override
    public <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> ConfigNumericEntry deserialize(Reader reader) throws TEx, MalformedDataException {
        ConfigNumericEntry result = new ConfigNumericEntry();
        if (reader.peek() == Token.NUMBER) {
            double value = reader.nextDouble();
            result.setValue(value);
            result.setDefault(value);
            return result;
        } else if (reader.peek() == Token.BEGIN_OBJECT) {
            reader.beginObject();
            while (reader.hasNext()) {
                String key = reader.nextName();
                switch (key) {
                    case "type" -> {
                        String type = reader.nextString();
                        if (!TYPES.contains(type)) {
                            throw new MalformedDataException("Invalid type for numeric entry");
                        }
                        if (INT_TYPES.contains(type)) {
                            result = result.asInteger();
                        }
                    }
                    case "default" -> {
                        double value = reader.nextDouble();
                        result.setDefault(value);
                        result.setValue(value);
                    }
                    case "reloadType" -> result.setReloadType(PackReloadType.valueOf(reader.nextString()));
                    case "min" -> result.setMin(reader.nextDouble());
                    case "max" -> result.setMax(reader.nextDouble());
                    default -> throw new MalformedDataException("Unknown key in numeric entry: " + key);
                }
            }
            reader.endObject();
            return result;
        } else {
            throw new MalformedDataException("Invalid data type for numeric entry");
        }
    }
}
