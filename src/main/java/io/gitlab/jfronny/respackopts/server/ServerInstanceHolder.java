package io.gitlab.jfronny.respackopts.server;

import com.google.common.collect.Lists;
import io.gitlab.jfronny.commons.ref.WeakSet;
import io.gitlab.jfronny.respackopts.Respackopts;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.minecraft.resource.ResourcePackManager;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.SaveProperties;

import java.util.List;
import java.util.Set;

public class ServerInstanceHolder {
    private static final Set<MinecraftServer> SERVERS = new WeakSet<>();

    public static void init() {
        ServerLifecycleEvents.SERVER_STARTED.register(server -> {
            if (!SERVERS.isEmpty()) {
                Respackopts.LOGGER.warn("Multiple servers started at once. This is unexpected");
            }
            if (!SERVERS.add(server)) {
                Respackopts.LOGGER.warn("Called SERVER_STARTED on a server that is already started");
            }
        });
        ServerLifecycleEvents.SERVER_STOPPED.register(server -> {
            if (!SERVERS.remove(server)) {
                Respackopts.LOGGER.warn("Called SERVER_STOPPED on a server that was never started/already stopped");
            }
            if (!SERVERS.isEmpty()) {
                Respackopts.LOGGER.warn("Server stopped but another one is still running. This is unexpected");
            }
        });
    }

    public static void reloadResources() {
        if (SERVERS.isEmpty()) {
            Respackopts.LOGGER.warn("Attempted to reload resources while no server is running. Skipping");
        }
        for (MinecraftServer server : SERVERS) {
            ResourcePackManager manager = server.getDataPackManager();
            SaveProperties saveProperties = server.getSaveProperties();
            List<String> enabled = Lists.newArrayList(manager.getEnabledIds());
            manager.scanPacks();
            List<String> disabled = saveProperties.getDataConfiguration().dataPacks().getDisabled();
            for (String name : manager.getIds()) {
                if (!disabled.contains(name) && !enabled.contains(name)) {
                    enabled.add(name);
                }
            }
            server.reloadResources(enabled).exceptionally(throwable -> {
                Respackopts.LOGGER.warn("Failed to reload data packs", throwable);
                return null;
            });
        }
    }
}
