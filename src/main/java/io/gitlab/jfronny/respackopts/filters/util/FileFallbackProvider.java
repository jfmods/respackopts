package io.gitlab.jfronny.respackopts.filters.util;

import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.model.cache.CacheKey;
import io.gitlab.jfronny.respackopts.muscript.RespackoptsFS;
import io.gitlab.jfronny.respackopts.util.MetaCache;
import net.minecraft.resource.InputSupplier;
import net.minecraft.resource.ResourcePack;
import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;

import java.io.InputStream;
import java.util.Set;

public class FileFallbackProvider {
    public static InputSupplier<InputStream> getReplacement(CacheKey key, RespackoptsFS fs, String file) {
        return FileRpoSearchProvider.modifyWithRpo(file, key, fs, rpo -> {
            if (rpo.fallbacks == null) return null;
            try {
                for (String s : rpo.fallbacks) {
                    MetaCache.addDependency(key, file, s);
                    InputSupplier<InputStream> is = fs.open(s);
                    if (is != null) return is;
                }
                Respackopts.LOGGER.error("Could not determine replacement for {0}", file);
            } catch (Exception e) {
                Respackopts.LOGGER.error("Could not determine replacement for " + file, e);
            }
            return null;
        }, null);
    }

    public static void addFallbackResources(String namespace, ResourceType type, Identifier identifier, CacheKey key, RespackoptsFS fs, Set<Identifier> ids, ResourcePack.ResultConsumer out) {
        // Warning: the Identifiers here DON'T CONTAIN THE TYPE! Therefore, it needs to be added when calling a method that generates a ResourcePath!
        String path = identifier.getPath();
        if (!FileRpoSearchProvider.isRpo(path)) return;
        String expectedTarget = path.substring(0, path.length() - Respackopts.FILE_EXTENSION.length());
        if (ids.stream().anyMatch(s -> s.getPath().equals(expectedTarget))) return;
        InputSupplier<InputStream> replacement = getReplacement(key, fs, type.getDirectory() + "/" + expectedTarget);
        if (replacement == null) return;
        Identifier id = Identifier.of(namespace, expectedTarget);
        ids.add(id);
        out.accept(id, replacement);
    }
}
