package io.gitlab.jfronny.respackopts.filters;

import io.gitlab.jfronny.commons.logger.SystemLoggerPlus;
import io.gitlab.jfronny.commons.unsafe.reflect.Reflect;
import io.gitlab.jfronny.libjf.ResourcePath;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.RespackoptsConfig;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.fabricmc.fabric.api.resource.SimpleSynchronousResourceReloadListener;
import net.minecraft.resource.*;
import net.minecraft.util.Identifier;

import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.SequencedMap;

public class ValidationLayer {
    private static final ThreadLocal<SequencedMap<Frame, Integer>> stack = ThreadLocal.withInitial(() -> null);
    private static final int MAX_RECURSION = 5;
    private static final SystemLoggerPlus LOG = SystemLoggerPlus.forName(Respackopts.ID + "/validationLayer");
    private static SequencedMap<Frame, Integer> stack() {
        SequencedMap<Frame, Integer> stack = ValidationLayer.stack.get();
        if (stack == null) {
            stack = new LinkedHashMap<>();
            ValidationLayer.stack.set(stack);
        }
        return stack;
    }

    private sealed interface Frame {
        record Resource(String name) implements Frame {
            @Override
            public String toString() {
                return name;
            }
        }
    }

    private static class ResetListener implements SimpleSynchronousResourceReloadListener {
        @Override
        public Identifier getFabricId() {
            return Identifier.of(Respackopts.ID, "validation_layer");
        }

        @Override
        public void reload(ResourceManager manager) {
            try {
                Thread[] threads = new Thread[Thread.activeCount() + 1];
                if (Thread.enumerate(threads) == threads.length) {
                    LOG.warn("Thread array too small, some threads may not be checked");
                }
                var fn = Reflect.instanceFunction(ThreadLocal.class, "get", Object.class, Thread.class);
                for (Thread thread : threads) {
                    if (thread == null) continue;
                    var value = fn.apply(ValidationLayer.stack, thread);
                    if (value instanceof SequencedMap<?, ?> map && !map.isEmpty()) {
                        LOG.info("Thread " + thread.getName() + " returned with stack " + map.keySet());
                        map.clear();
                    }
                }
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
    }

    @SuppressWarnings("unused") // fabric.mod.json
    public static class Pre implements IEvents {
        public Pre() {
            for (ResourceType value : ResourceType.values()) {
                ResourceManagerHelper.get(value).registerReloadListener(new ResetListener());
            }
        }

        @Override
        public InputSupplier<InputStream> open(InputSupplier<InputStream> previous, ResourcePack pack, String name) {
            if (!RespackoptsConfig.validationLayer) return previous;
            var stack = stack();
            int frame = stack.merge(new Frame.Resource(name), 1, Integer::sum);
            if (frame > MAX_RECURSION) {
                throw new RuntimeException("[validationLayer] Detected infinite recursion while trying to load " + name + ". Stack of resources was " + stack.keySet());
            } else if (frame > 1) {
                LOG.warn("Detected possible infinite recursion while trying to load {0}. Stack of resources was {1}", stack.firstEntry().getKey(), stack.keySet());
            }
            switch (RespackoptsConfig.scanState) {
                case NONE -> {
                    if (!isPermittedWithoutScan(pack, name)) {
                        LOG.warn("ResourcePack {0} tried to access {1} even though scanning has not started yet", pack.getId(), name);
                    }
                }
                case SCANNING -> {
                    if (!isPermittedDuringScan(pack, name)) {
                        LOG.warn("ResourcePack {0} tried to access {1} before scanning is done", pack.getId(), name);
                    }
                }
            }
            return previous;
        }

        private boolean isPermittedDuringScan(ResourcePack pack, String name) {
            if (isPermittedWithoutScan(pack, name)) return true;
            if (name.equals(Respackopts.ID + ".json5")) return true;
            for (ResourceType value : ResourceType.values()) {
                if (name.equals(new ResourcePath(value, Identifier.of(Respackopts.ID, "conf.json")).getName())) {
                    return true;
                }
            }
            for (String namespace : pack.getNamespaces(ResourceType.CLIENT_RESOURCES)) {
                if (name.equals(new ResourcePath(ResourceType.CLIENT_RESOURCES, Identifier.of(namespace, "lang/en_us.json")).getName())) {
                    return true;
                }
            }
            if (name.endsWith(Respackopts.FILE_EXTENSION)) return true;
            return false;
        }

        private boolean isPermittedWithoutScan(ResourcePack pack, String name) {
            return switch (name) {
                case "pack.mcmeta" -> true;
                case "icons/icon_16x16.png",
                     "icons/icon_32x32.png",
                     "icons/icon_48x48.png",
                     "icons/icon_128x128.png",
                     "icons/icon_256x256.png" -> pack instanceof DefaultResourcePack;
                default -> false;
            };
        }
    }

    @SuppressWarnings("unused") // fabric.mod.json
    public static class Post implements IEvents {
        @Override
        public InputSupplier<InputStream> open(InputSupplier<InputStream> previous, ResourcePack pack, String name) {
            if (!RespackoptsConfig.validationLayer) return previous;
            var stack = stack();
            stack.compute(new Frame.Resource(name), (k, v) -> {
                if (v == null) throw new IllegalStateException("[validationLayer] Frame not found for " + name);
                return v == 1 ? null : v - 1;
            });
            return previous;
        }
    }
}
