package io.gitlab.jfronny.respackopts.filters.util;

import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.model.Condition;
import io.gitlab.jfronny.respackopts.model.cache.CacheKey;
import io.gitlab.jfronny.respackopts.muscript.RespackoptsFS;
import io.gitlab.jfronny.respackopts.util.MetaCache;

public class FileExclusionProvider {
    public static boolean fileHidden(CacheKey key, RespackoptsFS fs, String file) {
        return FileRpoSearchProvider.modifyWithRpo(file, key, fs, rpo -> {
            if (rpo.condition == null)
                return false;
            try {
                return !rpo.condition.get(MetaCache.getScope(key, fs));
            } catch (Condition.ConditionException e) {
                String res = "Could not evaluate condition for " + file + " (pack: " + key.packName() + ")";
                try {
                    Respackopts.LOGGER.error(res + " with condition:\n" + rpo.condition + ")", e);
                } catch (Throwable ex) {
                    Respackopts.LOGGER.error(res, e);
                }
                return false;
            }
        }, false);
    }
}
