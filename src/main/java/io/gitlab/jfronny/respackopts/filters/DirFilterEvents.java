package io.gitlab.jfronny.respackopts.filters;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.ResourcePath;
import io.gitlab.jfronny.libjf.data.manipulation.api.ResourcePackInterceptor;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.filters.util.DirRpoResult;
import io.gitlab.jfronny.respackopts.serialization.AttachmentHolder;
import io.gitlab.jfronny.respackopts.model.DirRpo;
import io.gitlab.jfronny.respackopts.model.GC_DirRpo;
import io.gitlab.jfronny.respackopts.model.cache.CacheKey;
import io.gitlab.jfronny.respackopts.model.cache.CachedPackState;
import io.gitlab.jfronny.respackopts.model.enums.PackCapability;
import io.gitlab.jfronny.respackopts.muscript.RespackoptsFS;
import io.gitlab.jfronny.respackopts.util.MetaCache;
import net.minecraft.resource.InputSupplier;
import net.minecraft.resource.ResourcePack;
import net.minecraft.resource.ResourceType;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class DirFilterEvents implements IEvents {
    @Override
    public InputSupplier<InputStream> open(InputSupplier<InputStream> previous, ResourcePack pack, String name) {
        List<DirRpo> rpo = findDirRpos(pack, parent(name));
        CacheKey key = MetaCache.getKeyByPack(pack);
        RespackoptsFS fs = new RespackoptsFS(pack);
        return switch (DirRpoResult.compute(name, rpo, key, fs)) {
            case DirRpoResult.FixedState.ORIGINAL -> previous; // Using original file
            case DirRpoResult.FixedState.IGNORE -> null; // No fallback
            case DirRpoResult.Replacement replacement -> {
                // Use fallback
                String fallback = replacement.toFallback(name);
                MetaCache.addDependency(key, name, fallback);
                yield fs.open(fallback);
            }
        };
    }

    @Override
    public ResourcePack.ResultConsumer findResources(ResourceType type, String namespace, String prefix, ResourcePack.ResultConsumer previous, ResourcePack pack) {
        // Warning: the Identifiers here DON'T CONTAIN THE TYPE!
        // Therefore, it needs to be added when calling a method that generates a ResourcePath!
        if (skip(pack)) return previous;
        boolean dirFilterAdditive = MetaCache.hasCapability(pack, PackCapability.DirFilterAdditive);
        String searchPrefix = type.getDirectory() + "/" + namespace + "/" + prefix;
        Set<String> additionalSearched = new HashSet<>();
        CacheKey key = MetaCache.getKeyByPack(pack);
        RespackoptsFS fs = new RespackoptsFS(pack);
        return (identifier, value) -> {
            String path = new ResourcePath(type, identifier).getName();
            List<DirRpo> relevantRpos = findDirRpos(pack, parent(path));
            switch (DirRpoResult.compute(path, relevantRpos, key, fs)) {
                case DirRpoResult.FixedState.ORIGINAL -> {
                    // Using original file
                    previous.accept(identifier, value);
                }
                case DirRpoResult.FixedState.IGNORE -> {
                    // No fallback
                }
                case DirRpoResult.Replacement replacement -> {
                    // New search for fallback path
                    String newPath = replacement.toFallback(path);
                    if (newPath.split("/", 3).length != 3) {
                        Respackopts.LOGGER.error("Directory fallback path MUST be long enough to support representation as identifier (3 segments), but is too short: {0}", newPath);
                        return;
                    }
                    if (!dirFilterAdditive) {
                        // Only return this single result, don't search for others
                        MetaCache.addDependency(key, path, newPath);
                        previous.accept(identifier, fs.open(newPath));
                        return;
                    }
                    // Find other files in the fallback directory
                    String fallbackDir = replacement.fallback.prefix();
                    if (!additionalSearched.add(fallbackDir)) return; // Already searched
                    int prefixSize = replacement.original.prefix().length();
                    if (prefixSize < searchPrefix.length()) {
                        if (!searchPrefix.startsWith(replacement.original.prefix())) {
                            Respackopts.LOGGER.error("Unexpected prefix path {0} for search prefix {1}, skipping", replacement.original.prefix(), searchPrefix);
                            return;
                        }
                        fallbackDir += searchPrefix.substring(prefixSize);
                    } else if (!replacement.original.prefix().startsWith(searchPrefix)) {
                        Respackopts.LOGGER.error("Unexpected prefix path {0} for search prefix {1}, skipping", replacement.original.prefix(), searchPrefix);
                        return;
                    }
                    if (fallbackDir.split("/", 3).length != 3) {
                        Respackopts.LOGGER.error("Directory fallback path MUST be long enough to support representation as identifier (3 segments), but is too short: {0}", fallbackDir);
                        return;
                    }
                    ResourcePath rp = new ResourcePath(fallbackDir);
                    pack.findResources(rp.getType(), rp.getId().getNamespace(), rp.getId().getPath(), (resource, resVal) -> {
                        ResourceType type1 = rp.getType();
                        String fallbackPath = new ResourcePath(type1, resource).getName();
                        String orig = replacement.toOriginal(fallbackPath);
                        MetaCache.addDependency(key, orig, fallbackPath);
                        previous.accept(new ResourcePath(orig).getId(), resVal);
                    });
                }
            }
        };
    }

    private String parent(String path) {
        int li = path.lastIndexOf('/');
        return li <= 0 ? null : path.substring(0, li);
    }

    /**
     * Identify all directory RPOs relevant to the directory at {@code path} (IE in it or its parents), from outermost to innermost
     */
    private List<DirRpo> findDirRpos(ResourcePack pack, String path) {
        if (path == null) return List.of();

        CacheKey key = MetaCache.getKeyByPack(pack);
        RespackoptsFS fs = new RespackoptsFS(pack);
        CachedPackState state = MetaCache.getState(key);
        var cache = state.cachedDirRPOs();

        {
            // This is outside computeIfAbsent because it could cause modification of the map, which is unsupported within it
            List<DirRpo> cached = cache.get(path);
            if (cached != null) return cached;
        }

        List<DirRpo> parentRPOs = findDirRpos(pack, parent(path));
        synchronized (cache) { // This is synchronized as multiple resources might be accessed at the same time, potentially causing a CME here
            return cache.computeIfAbsent(path, $ -> {
                String rp = path + "/" + Respackopts.FILE_EXTENSION;
                InputSupplier<InputStream> is = ResourcePackInterceptor.disable(() -> fs.open(rp));
                if (is == null) return parentRPOs;
                if (state.tracker() != null) state.tracker().addDependency(path, rp);
                try (Reader w = new InputStreamReader(is.get())) {
                    List<DirRpo> currentRPOs = new LinkedList<>(parentRPOs);
                    DirRpo newRPO = AttachmentHolder.attach(state.metadata().version, () -> GC_DirRpo.deserialize(w, LibJf.LENIENT_TRANSPORT));
                    newRPO.hydrate(path);
                    if (newRPO.fallback != null && !newRPO.fallback.endsWith("/"))
                        newRPO.fallback += "/";
                    currentRPOs.add(newRPO);
                    return currentRPOs;
                } catch (IOException e) {
                    Respackopts.LOGGER.error("Couldn't open dir rpo {0}", rp, e);
                }
                return parentRPOs;
            });
        }
    }

    @Override
    public boolean skip(ResourcePack pack) {
        return !MetaCache.hasCapability(pack, PackCapability.DirFilter);
    }
}
