package io.gitlab.jfronny.respackopts.filters;

import io.gitlab.jfronny.commons.logger.SystemLoggerPlus;
import io.gitlab.jfronny.libjf.data.manipulation.api.ResourcePackInterceptor;
import io.gitlab.jfronny.respackopts.RespackoptsConfig;
import net.minecraft.resource.InputSupplier;
import net.minecraft.resource.ResourcePack;
import net.minecraft.resource.ResourceType;
import net.minecraft.resource.metadata.ResourceMetadataSerializer;
import net.minecraft.util.Identifier;

import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

public class IOLogEvents implements ResourcePackInterceptor {
    private static final SystemLoggerPlus LOG = SystemLoggerPlus.forName("respackopts/io");

    @Override
    public ResourcePack.ResultConsumer findResources(ResourceType type, String namespace, String prefix, ResourcePack.ResultConsumer previous, ResourcePack pack) {
        if (!RespackoptsConfig.ioLogs) return previous;
        LOG.info("FIND_RESOURCE " + type + " in " + namespace + " " + prefix + " of " + pack.getId());
        Set<Identifier> results = new HashSet<>();
        return (identifier, inputStreamInputSupplier) -> {
            if (!results.add(identifier)) {
                LOG.warn("Duplicate identifier returned by findResources: " + identifier);
            }
            previous.accept(identifier, inputStreamInputSupplier);
        };
    }

    @Override
    public InputSupplier<InputStream> open(ResourceType type, Identifier id, InputSupplier<InputStream> previous, ResourcePack pack) {
        if (RespackoptsConfig.ioLogs) LOG.info("OPEN " + type + " at " + id + " of " + pack.getId());
        return previous;
    }

    @Override
    public InputSupplier<InputStream> openRoot(String[] fileName, InputSupplier<InputStream> previous, ResourcePack pack) {
        if (RespackoptsConfig.ioLogs) LOG.info("OPEN_ROOT " + String.join("/", fileName) + " of " + pack.getId());
        return previous;
    }

    @Override
    public <T> T parseMetadata(ResourceMetadataSerializer<T> reader, Supplier<T> previous, ResourcePack pack) {
        if (RespackoptsConfig.ioLogs) LOG.info("PARSE_METADATA " + reader.name() + " of " + pack.getId());
        return previous.get();
    }
}
