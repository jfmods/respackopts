package io.gitlab.jfronny.respackopts.filters;

import io.gitlab.jfronny.libjf.ResourcePath;
import io.gitlab.jfronny.libjf.data.manipulation.api.ResourcePackInterceptor;
import net.minecraft.resource.InputSupplier;
import net.minecraft.resource.ResourcePack;
import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;

import java.io.InputStream;

public interface IEvents extends ResourcePackInterceptor {
    @Override
    default InputSupplier<InputStream> openRoot(String[] fileName, InputSupplier<InputStream> previous, ResourcePack pack) {
        if (skip(pack)) return previous;
        return open(previous, pack, String.join("/", fileName));
    }

    @Override
    default InputSupplier<InputStream> open(ResourceType type, Identifier id, InputSupplier<InputStream> previous, ResourcePack pack) {
        if (skip(pack)) return previous;
        return open(previous, pack, new ResourcePath(type, id).getName());
    }

    @Override
    default ResourcePack.ResultConsumer findResources(ResourceType type, String namespace, String prefix, ResourcePack.ResultConsumer previous, ResourcePack pack) {
        if (skip(pack)) return previous;
        return (identifier, value) -> {
            String path = new ResourcePath(type, identifier).getName();
            previous.accept(identifier, open(value, pack, path));
        };
    }

    InputSupplier<InputStream> open(InputSupplier<InputStream> previous, ResourcePack pack, String name);

    default boolean skip(ResourcePack pack) {
        return false;
    }
}
