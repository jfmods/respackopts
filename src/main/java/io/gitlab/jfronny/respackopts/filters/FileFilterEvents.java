package io.gitlab.jfronny.respackopts.filters;

import io.gitlab.jfronny.libjf.data.manipulation.api.ResourcePackInterceptor;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.filters.util.FileExclusionProvider;
import io.gitlab.jfronny.respackopts.filters.util.FileExpansionProvider;
import io.gitlab.jfronny.respackopts.filters.util.FileFallbackProvider;
import io.gitlab.jfronny.respackopts.model.cache.CacheKey;
import io.gitlab.jfronny.respackopts.model.enums.PackCapability;
import io.gitlab.jfronny.respackopts.muscript.RespackoptsFS;
import io.gitlab.jfronny.respackopts.util.MetaCache;
import net.minecraft.resource.InputSupplier;
import net.minecraft.resource.ResourcePack;
import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;

import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

public class FileFilterEvents implements IEvents {
    @Override
    public InputSupplier<InputStream> open(InputSupplier<InputStream> previous, ResourcePack pack, String name) {
        CacheKey key = MetaCache.getKeyByPack(pack);
        RespackoptsFS fs = new RespackoptsFS(pack);
        if (previous != null) {
            if (!FileExclusionProvider.fileHidden(key, fs, name)) return FileExpansionProvider.replace(previous, key, fs, name);
        } else {
            if (ResourcePackInterceptor.disable(() -> fs.open(name + Respackopts.FILE_EXTENSION)) != null) return null;
        }
        return FileFallbackProvider.getReplacement(key, fs, name);
    }

    @Override
    public ResourcePack.ResultConsumer findResources(ResourceType type, String namespace, String prefix, ResourcePack.ResultConsumer previous, ResourcePack pack) {
        // Warning: the Identifiers here DON'T CONTAIN THE TYPE!
        // Therefore, it needs to be added when calling a method that generates a ResourcePath!
        if (skip(pack)) return previous;
        Set<Identifier> ids = new HashSet<>();
        CacheKey key = MetaCache.getKeyByPack(pack);
        RespackoptsFS fs = new RespackoptsFS(pack);
        return (identifier, value) -> {
            // Completion of the path is handled separately here
            String fileName = type.getDirectory() + "/" + identifier.getNamespace() + "/" + identifier.getPath();
            if (!FileExclusionProvider.fileHidden(key, fs, fileName)) {
                ids.add(identifier);
                previous.accept(identifier, open(type, identifier, value, pack));
            } else {
                InputSupplier<InputStream> replacement = FileFallbackProvider.getReplacement(key, fs, fileName);
                if (replacement != null) {
                    ids.add(identifier);
                    previous.accept(identifier, replacement);
                }
            }
            FileFallbackProvider.addFallbackResources(namespace, type, identifier, key, fs, ids, previous);
        };
    }

    @Override
    public boolean skip(ResourcePack pack) {
        return !MetaCache.hasCapability(pack, PackCapability.FileFilter);
    }
}
