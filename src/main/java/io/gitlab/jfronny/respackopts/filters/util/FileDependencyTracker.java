package io.gitlab.jfronny.respackopts.filters.util;

import io.gitlab.jfronny.respackopts.Respackopts;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class FileDependencyTracker {
    private final String pack;
    private final Map<String, Set<String>> dependencies = new ConcurrentHashMap<>();
    private final Map<String, Set<String>> dependents = new ConcurrentHashMap<>();
    private final Set<String> reportedRecursions = ConcurrentHashMap.newKeySet();

    public FileDependencyTracker(String pack) {
        this.pack = pack;
    }

    public void addDependency(String to, String on) {
        if (to.equals(on)) {
            if (reportedRecursions.add(to)) Respackopts.LOGGER.warn("Discovered recursive dependency in {0}! If you get a StackOverflowException, please validate your fallbacks for {1}", pack, to);
            return;
        }
        gs(dependencies, to).add(on);
        gs(dependents, on).add(to);
        gs(dependents, to).forEach(dp -> addDependency(dp, on));
        gs(dependencies, on).forEach(dp -> addDependency(to, dp));
    }

    private Set<String> gs(Map<String, Set<String>> map, String key) {
        return map.computeIfAbsent(key, _1 -> new HashSet<>());
    }
}
