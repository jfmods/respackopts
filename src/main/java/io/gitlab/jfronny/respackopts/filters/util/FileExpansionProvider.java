package io.gitlab.jfronny.respackopts.filters.util;

import io.gitlab.jfronny.muscript.ast.StringExpr;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;
import io.gitlab.jfronny.muscript.data.dynamic.DObject;
import io.gitlab.jfronny.muscript.runtime.Runtime;
import io.gitlab.jfronny.respackopts.model.cache.CacheKey;
import io.gitlab.jfronny.respackopts.muscript.RespackoptsFS;
import io.gitlab.jfronny.respackopts.util.MetaCache;
import net.minecraft.resource.InputSupplier;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class FileExpansionProvider {
    public static synchronized InputStream replace(DObject parameter, InputStream is, Map<String, StringExpr> expansions) throws IOException {
        String s = new String(is.readAllBytes());
        for (Map.Entry<String, StringExpr> entry : expansions.entrySet()) {
            s = s.replace("${" + entry.getKey() + "}", Runtime.evaluate(entry.getValue(), Scope.of(parameter)));
        }
        return new ByteArrayInputStream(s.getBytes());
    }

    public static InputSupplier<InputStream> replace(InputSupplier<InputStream> inputStream, CacheKey key, RespackoptsFS fs, String file) {
        return FileRpoSearchProvider.modifyWithRpo(file, key, fs, rpo -> rpo.expansions == null || rpo.expansions.isEmpty() ? inputStream : () -> {
            try (InputStream is = inputStream.get()) {
                return replace(MetaCache.getScope(key, fs), is, rpo.expansions);
            }
        }, inputStream);
    }
}
