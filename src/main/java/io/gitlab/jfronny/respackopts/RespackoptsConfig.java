package io.gitlab.jfronny.respackopts;

import io.gitlab.jfronny.libjf.config.api.v2.*;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.ConfigBuilder;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.DSL;
import io.gitlab.jfronny.respackopts.util.MetaCache;
import net.fabricmc.loader.api.FabricLoader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

@JfConfig(tweaker = RespackoptsConfig.Tweaker.class)
public class RespackoptsConfig {
    @Entry public static boolean debugCommands = false;
    @Entry public static boolean debugLogs = false;
    @Entry public static boolean generateNames = true;
    @Entry public static boolean validationLayer = false;
    @Entry public static boolean ioLogs = false;

    public static ScanState scanState = ScanState.NONE;

    public enum ScanState {
        NONE,
        SCANNING,
        DONE
    }

    public static class Tweaker {
        public static ConfigBuilder<?> tweak(ConfigBuilder<?> builder) {
            Path dir = FabricLoader.getInstance().getConfigDir().resolve("respackopts");
            try {
                Files.createDirectories(dir);
            } catch (IOException e) {
                // Ignore for now
            }
            // Not using Respackopts.FALLBACK_CONF_DIR to avoid premature initialization with libjf-unsafe and libjf-config-reflect
            return builder.setPath(dir.resolve("_respackopts.conf"))
                    .referenceConfig(() -> {
                        if (scanState != ScanState.DONE) return List.of();
                        List<ConfigInstance> instances = new LinkedList<>();
                        MetaCache.forEach((key, state) -> instances.add(DSL.create(state.packId())
                                .config(cb -> state.configBranch().buildConfig(cb, key.dataLocation()))
                        ));
                        return instances;
                    });
        }
    }

    static {
        JFC_RespackoptsConfig.ensureInitialized();
    }
}
