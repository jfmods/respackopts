package io.gitlab.jfronny.respackopts.util;

public class RpoFormatException extends Exception {
    public RpoFormatException(String message) {
        super(message);
    }

    public RpoFormatException(String message, Throwable cause) {
        super(message, cause);
    }
}
