package io.gitlab.jfronny.respackopts.util;

public class IndentingStringBuilder {
    private final StringBuilder builder;
    private final String indent;

    public IndentingStringBuilder() {
        this.builder = new StringBuilder();
        this.indent = "";
    }

    public IndentingStringBuilder(StringBuilder builder, String indent) {
        this.builder = builder;
        this.indent = indent;
    }

    @Override
    public String toString() {
        if (builder.isEmpty()) return "";
        return builder.substring(1);
    }

    public IndentingStringBuilder line(String content) {
        builder.append('\n').append(indent).append(content.replace("\n", "\n" + indent));
        return this;
    }

    public IndentingStringBuilder indent() {
        return new IndentingStringBuilder(builder, indent + "    ");
    }
}
