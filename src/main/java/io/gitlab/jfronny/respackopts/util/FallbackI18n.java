package io.gitlab.jfronny.respackopts.util;

import net.minecraft.resource.ResourcePack;
import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;
import net.minecraft.util.Language;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class FallbackI18n {
    private static final Map<String, String> translations = new HashMap<>();

    public static void clear() {
        translations.clear();
    }

    public static Map<String, String> loadFrom(ResourcePack pack, String packId) {
        Map<String, String> translations = new HashMap<>();
        for (String namespace : pack.getNamespaces(ResourceType.CLIENT_RESOURCES)) {
            var translation = pack.open(ResourceType.CLIENT_RESOURCES, Identifier.of(namespace, "lang/en_us.json"));
            if (translation == null) continue;
            try (InputStream is = translation.get()) {
                Language.load(is, (key, value) -> {
                    if (key.startsWith("rpo." + packId + ".")) {
                        translations.put(key, value);
                    }
                });
            } catch (Throwable ignored) {
            }
        }
        return translations;
    }

    public static void add(Map<String, String> translations) {
        FallbackI18n.translations.putAll(translations);
    }

    public static void insertInto(Map<String, String> target) {
        translations.forEach(target::putIfAbsent);
    }
}
