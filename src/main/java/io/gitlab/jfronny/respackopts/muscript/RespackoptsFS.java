package io.gitlab.jfronny.respackopts.muscript;

import io.gitlab.jfronny.libjf.ResourcePath;
import io.gitlab.jfronny.muscript.core.SourceFS;
import io.gitlab.jfronny.respackopts.Respackopts;
import net.minecraft.resource.InputSupplier;
import net.minecraft.resource.ResourcePack;
import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;

public class RespackoptsFS implements SourceFS {
    private final ResourcePack pack;

    public RespackoptsFS(ResourcePack pack) {
        this.pack = pack;
    }

    @Override
    public @Nullable String read(String file) {
        InputSupplier<InputStream> op = open(file);
        if (op == null) return null;
        try (InputStream is = op.get()) {
            return new String(is.readAllBytes());
        } catch (IOException e) {
            Respackopts.LOGGER.error("Could not open file", e);
            return null;
        }
    }

    public @Nullable InputSupplier<InputStream> open(String file) {
        String[] segments = file.split("/");
        if (segments.length < 3) return pack.openRoot(segments);
        ResourcePath path = new ResourcePath(file);
        return open(path.getType(), path.getId());
    }

    public @Nullable InputSupplier<InputStream> open(ResourceType type, Identifier id) {
        return pack.open(type, id);
    }
}
