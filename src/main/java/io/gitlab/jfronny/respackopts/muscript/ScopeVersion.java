package io.gitlab.jfronny.respackopts.muscript;

import io.gitlab.jfronny.muscript.core.MuScriptVersion;

public enum ScopeVersion {
    V10(MuScriptVersion.V1),
    V12(MuScriptVersion.V2),
    V13(MuScriptVersion.V3);

    public final MuScriptVersion muScriptVersion;

    ScopeVersion(MuScriptVersion muScriptVersion) {
        this.muScriptVersion = muScriptVersion;
    }

    public static ScopeVersion by(int version) {
        return version < 10 ? V10 : version <= 12 ? V12 : V13;
    }

    public boolean contains(ScopeVersion version) {
        return compareTo(version) >= 0;
    }
}
