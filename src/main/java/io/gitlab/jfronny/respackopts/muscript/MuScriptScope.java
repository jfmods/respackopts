package io.gitlab.jfronny.respackopts.muscript;

import io.gitlab.jfronny.muscript.core.MuScriptVersion;
import io.gitlab.jfronny.muscript.core.SourceFS;
import io.gitlab.jfronny.muscript.data.additional.DFinal;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;
import io.gitlab.jfronny.muscript.data.additional.libs.IOLib;
import io.gitlab.jfronny.muscript.data.additional.libs.IOWrapper;
import io.gitlab.jfronny.muscript.data.additional.libs.StandardLib;
import io.gitlab.jfronny.muscript.json.JsonLib;
import io.gitlab.jfronny.respackopts.model.cache.CachedPackState;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.VersionParsingException;
import net.fabricmc.loader.api.metadata.version.VersionPredicate;

import java.util.EnumMap;
import java.util.Map;

public class MuScriptScope {
    private static final Map<ScopeVersion, Scope> map = new EnumMap<>(ScopeVersion.class);

    static {
        for (ScopeVersion version : ScopeVersion.values()) {
            Scope scope = StandardLib.createScope(version.muScriptVersion);
            scope = scope.set("version", args -> {
                if (args.size() != 2) throw new IllegalArgumentException("Expected 2 arguments on version but got " + args.size());
                VersionPredicate predicate;
                try {
                    predicate = VersionPredicate.parse(args.get(1).asString().getValue());
                } catch (VersionParsingException e) {
                    throw new IllegalArgumentException("Could not parse version predicate", e);
                }
                return DFinal.of(FabricLoader.getInstance().getModContainer(args.get(0).asString().getValue())
                        .map(c -> predicate.test(c.getMetadata().getVersion()))
                        .orElse(false));
            });
            if (version.contains(ScopeVersion.V13)) scope = JsonLib.addTo(scope);
            map.put(version, scope);
        }
    }

    public static Scope fork(int version) {
        return map.get(ScopeVersion.by(version)).fork();
    }

    public static Scope configureFS(Scope scope, CachedPackState state, SourceFS fs) {
        MuScriptVersion version = ScopeVersion.by(state.metadata().version).muScriptVersion;
        return IOLib.addTo(version, scope, new IOWrapper.Caching(new IOWrapper.SourceFSWrapper(version, fs)));
    }
}
