package io.gitlab.jfronny.respackopts.test;

import io.gitlab.jfronny.respackopts.model.PackMeta;
import io.gitlab.jfronny.respackopts.model.cache.CacheKey;
import io.gitlab.jfronny.respackopts.model.tree.*;
import io.gitlab.jfronny.respackopts.model.enums.ConfigSyncMode;
import net.fabricmc.loader.api.FabricLoader;
import org.junit.jupiter.api.AfterEach;
import io.gitlab.jfronny.respackopts.util.MetaCache;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;

import static io.gitlab.jfronny.respackopts.Respackopts.*;
import static org.junit.jupiter.api.Assertions.*;

class ConfigTreeTest {
    private static final String testEntryName = "test";
    private static final String testEntry1Name = "test1";
    private static CacheKey cacheKey;

    @BeforeAll
    static void initialize() {
        assertDoesNotThrow(() -> Files.deleteIfExists(FALLBACK_CONF_DIR.resolve(testEntry1Name + ".json")));
        assertDoesNotThrow(() -> Files.createDirectories(FALLBACK_CONF_DIR));
        LOGGER.info(FALLBACK_CONF_DIR.toString());
    }

    @AfterEach
    void reset() {
        MetaCache.clear();
        MetaCache.save(Arguments.DO_NOTHING);
        assertEquals(MetaCache.size(), 0);
    }

    @BeforeEach
    void initSingle() {
        cacheKey = addEmptyBranch();
        assertEquals(MetaCache.size(), 1);
    }

    private CacheKey addEmptyBranch() {
        return MetaCache.addFromScan(
                testEntryName,
                testEntryName,
                new PackMeta(
                        testEntryName,
                        META_VERSION,
                        new ConfigBranch()
                ),
                FabricLoader.getInstance().getConfigDir().resolve("respackopts-test")
        );
    }

    @Test
    void saveLoadSimple() {
        MetaCache.getBranch(cacheKey).add(testEntry1Name, new ConfigBooleanEntry(false));
        assertTrue(MetaCache.getBranch(cacheKey).has(testEntry1Name));
        assertFalse((Boolean) MetaCache.getBranch(cacheKey).get(testEntry1Name).getValue());

        MetaCache.save(Arguments.DO_NOTHING);
        MetaCache.remove(cacheKey);
        assertEquals(MetaCache.size(), 0);
        addEmptyBranch();

        MetaCache.getBranch(cacheKey).add(testEntry1Name, new ConfigBooleanEntry(false));
        MetaCache.load(cacheKey);
        assertEquals(MetaCache.size(), 1);
        assertTrue(MetaCache.getBranch(cacheKey).has(testEntry1Name));
        assertFalse((Boolean) MetaCache.getBranch(cacheKey).get(testEntry1Name).getValue());
    }

    @Test
    void syncSimple() {
        ConfigBranch test = new ConfigBranch();
        test.add(testEntry1Name, new ConfigBooleanEntry(false));
        MetaCache.getBranch(cacheKey).sync(test, ConfigSyncMode.RESPACK_LOAD);
        MetaCache.save(Arguments.DO_NOTHING);
        MetaCache.load(cacheKey);
        assertFalse((Boolean) MetaCache.getBranch(cacheKey).get(testEntry1Name).getValue());
        assertEquals(MetaCache.size(), 1);
        assertEquals(test.getValue().size(), 1);
        assertNotNull(MetaCache.getBranch(cacheKey));
        assertInstanceOf(ConfigBooleanEntry.class, test.get(testEntry1Name))
                .setValue(true);
        assertFalse((Boolean) MetaCache.getBranch(cacheKey).get(testEntry1Name).getValue());
        LOGGER.info("E");
        MetaCache.getBranch(cacheKey).sync(test, ConfigSyncMode.RESPACK_LOAD);
        assertFalse((Boolean) MetaCache.getBranch(cacheKey).get(testEntry1Name).getValue());
        MetaCache.getBranch(cacheKey).sync(test, ConfigSyncMode.CONF_LOAD);
        MetaCache.save(Arguments.DO_NOTHING);
        MetaCache.load(cacheKey);
        assertTrue((Boolean) MetaCache.getBranch(cacheKey).get(testEntry1Name).getValue());
    }

    @Test
    void syncWrongType() {
        ConfigBranch cb = new ConfigBranch();
        ConfigBranch cbNew = new ConfigBranch();
        cb.add(testEntryName, new ConfigBooleanEntry(false));
        cbNew.add(testEntryName, new ConfigNumericEntry());
        LOGGER.info("Expecting warning message");
        cbNew.sync(cb, ConfigSyncMode.RESPACK_LOAD);
        LOGGER.info("Expected warning end");
        cbNew.add(testEntryName, new ConfigBooleanEntry(true));
        cbNew.sync(cb, ConfigSyncMode.RESPACK_LOAD);
    }
}
