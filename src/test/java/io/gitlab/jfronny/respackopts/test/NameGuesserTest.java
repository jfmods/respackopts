package io.gitlab.jfronny.respackopts.test;

import io.gitlab.jfronny.respackopts.model.naming.NameGuesser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class NameGuesserTest {
    @Test
    public void testCamelCaseInput() {
        assertEquals("This Is A Sample String", NameGuesser.guess("thisIsASampleString"));
    }

    @Test
    public void testAcronymWithString() {
        assertEquals("UI String Formatter", NameGuesser.guess("UIStringFormatter"));
    }

    @Test
    public void testAcronymWithString2() {
        assertEquals("JSON Parser", NameGuesser.guess("JSONParser"));
    }

    @Test
    public void testAcronymWithString3() {
        assertEquals("HTTP Request Handler", NameGuesser.guess("HTTPRequestHandler"));
    }

    @Test
    public void testSingleLowerCaseWord() {
        assertEquals("Sample", NameGuesser.guess("sample"));
    }

    @Test
    public void testSingleUpperCaseWord() {
        assertEquals("XML", NameGuesser.guess("XML"));
    }

    @Test
    public void testEmptyString() {
        assertEquals("", NameGuesser.guess(""));
    }

    @Test
    public void testNullInput() {
        assertNull(NameGuesser.guess(null));
    }

    @Test
    public void testAllUpperCaseWithoutSpaces() {
        assertEquals("XMLHTTP Request", NameGuesser.guess("XMLHTTPRequest"));
    }

    @Test
    public void testMixedCaseWithNumbers() {
        assertEquals("Parse XML 1 And 2 JSON Objects", NameGuesser.guess("parseXML1And2JSONObjects"));
    }

    @Test
    public void testSingleCharacterLowerCase() {
        assertEquals("A", NameGuesser.guess("a"));
    }

    @Test
    public void testSingleCharacterUpperCase() {
        assertEquals("A", NameGuesser.guess("A"));
    }

    @Test
    public void testCamelCaseWithConsecutiveUppercaseLetters() {
        assertEquals("User JSON Data Parser", NameGuesser.guess("userJSONDataParser"));
    }

    @Test
    public void testTrailingUppercaseLetters() {
        assertEquals("Parse HTTP", NameGuesser.guess("ParseHTTP"));
    }

    @Test
    public void testSnakeCaseInput() {
        assertEquals("This Is A Sample String", NameGuesser.guess("this_is_a_sample_string"));
    }

    @Test
    public void testSnakeCaseWithNumbers() {
        assertEquals("Parse Xml 1 And 2 Json Objects", NameGuesser.guess("parse_xml_1_and_2_json_objects"));
    }

    @Test
    public void testScreamSnakeCase() {
        assertEquals("THIS IS A SAMPLE STRING", NameGuesser.guess("THIS_IS_A_SAMPLE_STRING"));
    }

    @Test
    public void testKebebCase() {
        assertEquals("This Is A Sample String", NameGuesser.guess("this-is-a-sample-string"));
    }

    @Test
    public void testPascalCase() {
        assertEquals("This Is A Sample String", NameGuesser.guess("ThisIsASampleString"));
    }
}
