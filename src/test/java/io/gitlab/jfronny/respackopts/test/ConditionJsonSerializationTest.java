package io.gitlab.jfronny.respackopts.test;

import io.gitlab.jfronny.commons.serialize.json.JsonReader;
import io.gitlab.jfronny.muscript.ast.BoolExpr;
import io.gitlab.jfronny.respackopts.serialization.AttachmentHolder;
import io.gitlab.jfronny.respackopts.serialization.BoolExprTypeAdapter;
import io.gitlab.jfronny.respackopts.util.MetaCache;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringReader;

import static io.gitlab.jfronny.muscript.runtime.Runtime.evaluate;
import static org.junit.jupiter.api.Assertions.*;

class ConditionJsonSerializationTest {
    @Test
    void gsonConditionSimple() {
        assertTrue(assertDoesNotThrow(() -> evaluateCondition("true")));
        assertTrue(assertDoesNotThrow(() -> evaluateCondition("{\"not\": false}")));
        assertTrue(assertDoesNotThrow(() -> evaluateCondition("[true, {\"not\": false}]")));
        assertFalse(assertDoesNotThrow(() -> evaluateCondition("[true, {\"not\": true}]")));
    }

    @Test
    void gsonConditionXor() {
        assertTrue(assertDoesNotThrow(() -> evaluateCondition("{\"xor\": [true, false]}")));
        assertFalse(assertDoesNotThrow(() -> evaluateCondition("{\"xor\": [true, true]}")));
        assertFalse(assertDoesNotThrow(() -> evaluateCondition("{\"xor\": [false, false]}")));
    }

    @Test
    void gsonConditionOr() {
        assertTrue(assertDoesNotThrow(() -> evaluateCondition("{\"or\": [true, false]}")));
        assertTrue(assertDoesNotThrow(() -> evaluateCondition("{\"or\": [true, true]}")));
        assertFalse(assertDoesNotThrow(() -> evaluateCondition("{\"or\": [false, false]}")));
    }

    @Test
    void gsonConditionNor() {
        assertFalse(assertDoesNotThrow(() -> evaluateCondition("{\"nor\": [true, false]}")));
        assertFalse(assertDoesNotThrow(() -> evaluateCondition("{\"nor\": [true, true]}")));
        assertTrue(assertDoesNotThrow(() -> evaluateCondition("{\"nor\": [false, false]}")));
    }

    @Test
    void gsonConditionAnd() {
        assertFalse(assertDoesNotThrow(() -> evaluateCondition("{\"and\": [true, false]}")));
        assertTrue(assertDoesNotThrow(() -> evaluateCondition("{\"and\": [true, true]}")));
        assertFalse(assertDoesNotThrow(() -> evaluateCondition("{\"and\": [false, false]}")));
    }

    private boolean evaluateCondition(String json) throws IOException {
        final int version = 7; // this is the last version supporting the old JSON format
        BoolExpr condition;
        try (var read = new StringReader(json); var ser = new JsonReader(read)) {
            condition = AttachmentHolder.attach(version, () -> BoolExprTypeAdapter.INSTANCE.deserialize(ser));
        }
        return evaluate(condition, MetaCache.getScope(version));
    }
}
