package io.gitlab.jfronny.respackopts.test;

import io.gitlab.jfronny.respackopts.model.tree.ConfigBranch;
import io.gitlab.jfronny.respackopts.model.tree.ConfigEntry;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Map;

import static io.gitlab.jfronny.respackopts.test.TestArgs.getBranch;
import static org.junit.jupiter.api.Assertions.*;

public class CopyTest {
    @Test
    void copyHashCode() throws IOException {
        assertEquals(getBranch().hashCode(), getBranch().clone().hashCode());
    }

    @Test
    void copyEquals() throws IOException{
        assertEquals(getBranch(), getBranch().clone());
    }

    @Test
    void parentTest() throws IOException {
        ConfigBranch cb = getBranch();
        ConfigBranch cb1 = cb.clone();
        for (Map.Entry<String, ConfigEntry<?>> entry : cb.getValue().entrySet()) {
            assertTrue(cb1.getValue().containsKey(entry.getKey()));
            assertEquals(entry.getValue(), cb1.getValue().get(entry.getKey()));
            assertEquals(cb, entry.getValue().getParent());
            assertEquals(cb1, cb1.getValue().get(entry.getKey()).getParent());
            if (entry.getValue() instanceof ConfigBranch cb2) {
                assertInstanceOf(ConfigBranch.class, cb1.getValue().get(entry.getKey()));
                ConfigBranch cb3 = (ConfigBranch) cb1.getValue().get(entry.getKey());
                for (Map.Entry<String, ConfigEntry<?>> entry2 : cb2.getValue().entrySet()) {
                    assertTrue(cb3.getValue().containsKey(entry2.getKey()));
                    assertEquals(entry2.getValue(), cb3.getValue().get(entry2.getKey()));
                    assertEquals(cb2, entry2.getValue().getParent());
                    assertEquals(cb3, cb3.getValue().get(entry2.getKey()).getParent());
                }
            }
        }
        assertInstanceOf(ConfigBranch.class, cb.get("subBranch"));
        assertEquals("subBranch.boolean3", ((ConfigBranch)cb.get("subBranch")).get("boolean3").getName());
    }
}
