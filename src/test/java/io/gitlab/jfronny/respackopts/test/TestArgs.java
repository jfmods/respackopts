package io.gitlab.jfronny.respackopts.test;

import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.respackopts.model.tree.*;

import java.io.IOException;

public class TestArgs {
    public static ConfigBranch getBranch() throws IOException {
        return GC_ConfigBranch.deserialize("""
                {
                  "boolean": true,
                  "boolean2": false,
                  "number1": 0,
                  "number2": 0.5,
                  "number3": {
                    "type": "slider",
                    "default": 2,
                    "min": 0,
                    "max": 4
                  },
                  "enum": [
                    "enum1",
                    "enum2",
                    "enum3"
                  ],
                  "subBranch": {
                    "boolean3": true,
                    "boolean4": false
                  }
                }
                """, LibJf.LENIENT_TRANSPORT);
    }
}
