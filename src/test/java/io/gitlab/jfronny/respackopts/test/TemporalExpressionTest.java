package io.gitlab.jfronny.respackopts.test;

import io.gitlab.jfronny.muscript.core.MuScriptVersion;
import io.gitlab.jfronny.muscript.data.additional.context.Scope;
import io.gitlab.jfronny.muscript.parser.Parser;
import io.gitlab.jfronny.respackopts.Respackopts;
import io.gitlab.jfronny.respackopts.util.MetaCache;
import org.junit.jupiter.api.Test;

import static io.gitlab.jfronny.muscript.ast.context.ExprUtils.asBool;
import static io.gitlab.jfronny.muscript.ast.context.ExprUtils.asNumber;
import static io.gitlab.jfronny.muscript.runtime.Runtime.evaluate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TemporalExpressionTest {

    @Test
    void componentTest() {
        Scope param = MetaCache.getScope(Respackopts.META_VERSION);

        assertEquals(10, evaluate(asNumber(Parser.parse(MuScriptVersion.DEFAULT, "date(2020, 5, 10).day")), param));
        assertEquals(5, evaluate(asNumber(Parser.parse(MuScriptVersion.DEFAULT, "date(2020, 5, 10).month")), param));
        assertEquals(2020, evaluate(asNumber(Parser.parse(MuScriptVersion.DEFAULT, "date(2020, 5, 10).year")), param));

        assertEquals(59, evaluate(asNumber(Parser.parse(MuScriptVersion.DEFAULT, "time(13, 45, 59).second")), param));
        assertEquals(45, evaluate(asNumber(Parser.parse(MuScriptVersion.DEFAULT, "time(13, 45, 59).minute")), param));
        assertEquals(13, evaluate(asNumber(Parser.parse(MuScriptVersion.DEFAULT, "time(13, 45, 59).hour")), param));
    }

    @Test
    void compareTest() {

        Scope param = MetaCache.getScope(Respackopts.META_VERSION);

        assertTrue(evaluate(asBool(Parser.parse(MuScriptVersion.DEFAULT, "date(2020, 5, 10) > date(2020, 5, 9)")), param));
        assertTrue(evaluate(asBool(Parser.parse(MuScriptVersion.DEFAULT, "date(2020, 5, 10) > date(2020, 4, 10)")), param));
        assertTrue(evaluate(asBool(Parser.parse(MuScriptVersion.DEFAULT, "date(2020, 5, 10) > date(2019, 5, 10)")), param));

        assertTrue(evaluate(asBool(Parser.parse(MuScriptVersion.DEFAULT, "time(13, 45, 59) > time(13, 45, 58)")), param));
        assertTrue(evaluate(asBool(Parser.parse(MuScriptVersion.DEFAULT, "time(13, 45, 59) > time(13, 44, 59)")), param));
        assertTrue(evaluate(asBool(Parser.parse(MuScriptVersion.DEFAULT, "time(13, 45, 59) > time(12, 45, 59)")), param));
    }
}
